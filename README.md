# Virtual Teacher

A platform for online tutoring

Virtual Teacher is a free platform for online tutoring. 
The application supports users who can have different roles.
When a user is registering he can choose to register as a student or a teacher.
Depending on the chosen role and if your request is approved by a administrator
users have different levels of access.
Students can enroll for courses. Have access to his enrolled and completed courses,
and his profile page where if he wants he can request to be a teacher on the platform.
After student watches the video lecture he can submit an assignment for grading. 
Teachers have access to all the functionalities that students do. They can create courses,
modify courses, create lectures, grade assignments, and search and view students' profiles.
Teachers have the possibility to request admin rights. And this is the only
way to gain such rights.
Administrators have access to all the functionalities that 
teacher does plus the ability to delete a course or a user.


In order to run the application,  you need to download and install IntelliJ Ultimate.
You will need JDK 11.
Download source code from Git repository and load it in IntelliJ Ultimate.
You have to download and install RDBMS MariaDB and load the Date Base script and the filler script. 
Run the application from IntelliJ Ultimate.

SWAGGER link: http://localhost:8080/swagger-ui/index.html#/

Data Base structure and connections:

![virtual_teacher_data_base](/uploads/f2db3280762916186ab03fe1bb3dad25/virtual_teacher_data_base.png)

