-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.4-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for virtual_teacher
CREATE DATABASE IF NOT EXISTS `virtual_teacher` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `virtual_teacher`;

-- Dumping structure for table virtual_teacher.assignment
CREATE TABLE IF NOT EXISTS `assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `lecture_id` int(11) NOT NULL,
  `file_path` text DEFAULT NULL,
  `isComplete` tinyint(1) NOT NULL DEFAULT 0,
  `user_grade` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `assignment_users_user_id_fk` (`user_id`),
  KEY `assignment_lectures_lecture_id_fk` (`lecture_id`),
  KEY `assignment_courses_course_id_fk` (`course_id`),
  CONSTRAINT `assignment_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `assignment_lectures_lecture_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`),
  CONSTRAINT `assignment_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.courses
CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `min_grade` double DEFAULT 0,
  `picture_url` text DEFAULT NULL,
  `rating` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`course_id`),
  UNIQUE KEY `courses_title_uindex` (`title`),
  KEY `courses_topics_topic_id_fk` (`topic_id`),
  CONSTRAINT `courses_topics_topic_id_fk` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.course_lectures
CREATE TABLE IF NOT EXISTS `course_lectures` (
  `course_id` int(11) NOT NULL,
  `lecture_id` int(11) NOT NULL,
  KEY `course_lectures_courses_course_id_fk` (`course_id`),
  KEY `course_lectures_lectures_lecture_id_fk` (`lecture_id`),
  CONSTRAINT `course_lectures_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `course_lectures_lectures_lecture_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.course_teachers
CREATE TABLE IF NOT EXISTS `course_teachers` (
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `course_teachers_courses_course_id_fk` (`course_id`),
  KEY `course_teachers_users_user_id_fk` (`user_id`),
  CONSTRAINT `course_teachers_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `course_teachers_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.lectures
CREATE TABLE IF NOT EXISTS `lectures` (
  `lecture_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `video_url` text NOT NULL,
  `assignment_description` varchar(500) NOT NULL,
  PRIMARY KEY (`lecture_id`),
  UNIQUE KEY `lectures_title_uindex` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.lecture_notes
CREATE TABLE IF NOT EXISTS `lecture_notes` (
  `note_id` int(11) NOT NULL,
  `lecture_id` int(11) NOT NULL,
  KEY `lecture_notes_lectures_lecture_id_fk` (`lecture_id`),
  KEY `lecture_notes_notes_note_id_fk` (`note_id`),
  CONSTRAINT `lecture_notes_lectures_lecture_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`),
  CONSTRAINT `lecture_notes_notes_note_id_fk` FOREIGN KEY (`note_id`) REFERENCES `notes` (`note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.notes
CREATE TABLE IF NOT EXISTS `notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `roles_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.role_requests
CREATE TABLE IF NOT EXISTS `role_requests` (
  `role_request_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`role_request_id`),
  KEY `role_requests_roles_role_id_fk` (`role_id`),
  KEY `role_requests_users_user_id_fk` (`user_id`),
  CONSTRAINT `role_requests_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `role_requests_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.topics
CREATE TABLE IF NOT EXISTS `topics` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`topic_id`),
  UNIQUE KEY `topic_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `profile_picture` text DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `about_user` text DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.user_courses
CREATE TABLE IF NOT EXISTS `user_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `rating` double DEFAULT NULL,
  `final_grade` double NOT NULL DEFAULT 0,
  `isComplete` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `user_courses_courses_course_id_fk` (`course_id`),
  KEY `user_courses_users_user_id_fk` (`user_id`),
  CONSTRAINT `user_courses_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `user_courses_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table virtual_teacher.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `user_roles_roles_role_id_fk` (`role_id`),
  KEY `user_roles_users_user_id_fk` (`user_id`),
  CONSTRAINT `user_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `user_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
