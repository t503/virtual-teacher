INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (25, 'Yordan', 'Nikolov', 'nikolov.y.b@gmail.com', 'Qwerty123#', '/uploads/1633635741983_1632866593244_yordan.jpeg', '0876668786', 'Admin for Virtual teacher for 1 month now.');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (26, 'Petar', 'Valchev', 'petar.valchev123@gmail.com', 'Qwerty123#', '/uploads/1633630774985_pvvv.jpg', '0899926567', 'Teaching at Virtual teacher for 1 month.');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (27, 'John', 'Doe', 'john.doe@gmail.com', 'Qwerty123#', 'aeecc22a67dac7987a80ac0724658493.jpg', '0897213621', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (28, 'Mark', 'Smith', 'mark.smith@gmail.com', 'Qwerty123#', '/uploads/images (1).jfif', '0876527210', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (29, 'Ivan', 'Ivanovich', 'ivan.ivanovich@gmail.com', 'Qwerty123#', '/uploads/download.jfif', '0897213621', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (30, 'Venelin', 'Kuzmanov', 'venelin.kuzmanov@gmail.com', 'Qwerty123#', '/uploads/lSvWN_q-_400x400.jpg', '0872648562', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (31, 'Bruce', 'Wayne', 'bruce.wayne@wenterprice.com', 'Qwerty123#', '/uploads/4cc6f1f365fce288fcae170fa17b6875.jpg', '0987654321', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (32, 'Katya', 'Avramova', 'katya.avramova@gmail.com', 'Qwerty123#', '/uploads/08.jpg', '0897654657', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (33, 'Violeta', 'Penkova', 'vili.penkova@gmail.com', 'Qwerty123#', '/uploads/images.jfif', '0876527210', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (34, 'Maria', 'Nikiforova', 'maria.nikiforova@gmail.com', 'Qwerty123#', '/uploads/random-user-31.jpg', '0897213621', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (35, 'Dwayne', 'Johnson', 'dwayne.johnson@rock.com', 'Qwerty123#', '/uploads/dwayne-fanny-pack.jpg', '0872648562', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (36, 'Martin', 'Martinov', 'martinov@gmail.com', 'Qwerty123#', null, '0987654321', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');
INSERT INTO virtual_teacher.users (user_id, first_name, last_name, email, password, profile_picture, phone_number, about_user) VALUES (37, 'Nikolay', 'Ivanov', 'niki.ivanov@gmail.com', 'Qwerty123#', null, '0897654657', 'Junior Java Developer looking to upgrade his skill set.
Currently skilled in:
Java
Hibernate
Spring
HTML
CSS');


INSERT INTO virtual_teacher.roles (role_id, name) VALUES (3, 'Administrator');
INSERT INTO virtual_teacher.roles (role_id, name) VALUES (1, 'Student');
INSERT INTO virtual_teacher.roles (role_id, name) VALUES (2, 'Teacher');


INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (26, 2);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (29, 2);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (27, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (28, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (29, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (30, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (31, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (32, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (33, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (34, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (35, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (36, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (37, 1);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (25, 2);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (25, 3);
INSERT INTO virtual_teacher.user_roles (user_id, role_id) VALUES (26, 1);

INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (7, 'Algorithms');
INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (6, 'Data Structures');
INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (8, 'Frameworks');
INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (1, 'Object Oriented Programing');
INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (4, 'Object Relational Mapping');
INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (5, 'RDBMS');
INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (3, 'Template Engine');
INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (2, 'UI Design');
INSERT INTO virtual_teacher.topics (topic_id, name) VALUES (9, 'WEB');


INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (16, 'Java', 1, 'Java is the #1 programming language and development platform. It reduces costs, shortens development timeframes, drives innovation, and improves application services. With millions of developers running more than 51 billion Java Virtual Machines worldwide, Java continues to be the development platform of choice for enterprises and developers.Master Java with IT Virtual Teacher course!', '2021-10-04', 4.3, '/assets/img/portfolio/java.jpg', 0);
INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (17, 'Spring', 8, 'The Spring Framework is an application framework and inversion of control container for the Java platform. The framework''s core features can be used by any Java application, but there are extensions for building web applications on top of the Java EE (Enterprise Edition) platform. Although the framework does not impose any specific programming model, it has become popular in the Java community as an addition to the Enterprise JavaBeans (EJB) model. The Spring Framework is open source.', '2021-10-04', 4, '/assets/img/portfolio/spring.jpg', 0);
INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (18, 'Maria DB', 5, 'MariaDB is a community-developed, commercially supported fork of the MySQL relational database management system (RDBMS), intended to remain free and open-source software under the GNU General Public License. Development is led by some of the original developers of MySQL, who forked it due to concerns over its acquisition by Oracle Corporation in 2009', '2021-10-04', 4.1, '/assets/img/portfolio/MariaDB.jpg', 0);
INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (19, 'Thymeleaf', 3, 'Thymeleaf is a modern server-side Java template engine for both web and standalone environments. Thymeleaf''s main goal is to bring elegant natural templates to your development workflow — HTML that can be correctly displayed in browsers and also work as static prototypes, allowing for stronger collaboration in development teams. With modules for Spring Framework, a host of integrations with your favourite tools, and the ability to plug in your own functionality, Thymeleaf is ideal for modern-day HTML5 JVM web development — although there is much more it can do.', '2021-10-04', 4.4, '/assets/img/portfolio/thymeleaf.jpg', 0);
INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (20, 'HTML & CSS', 2, 'The HyperText Markup Language, or HTML is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets (CSS) and scripting languages such as JavaScript. Web browsers receive HTML documents from a web server or from local storage and render the documents into multimedia web pages. HTML describes the structure of a web page semantically and originally included cues for the appearance of the document.', '2021-10-04', 4.25, '/assets/img/portfolio/html-css.jpg', 0);
INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (21, 'Hibernate', 4, 'Hibernate ORM (or simply Hibernate) is an object–relational mapping tool for the Java programming language. It provides a framework for mapping an object-oriented domain model to a relational database. Hibernate handles object–relational impedance mismatch problems by replacing direct, persistent database accesses with high-level object handling functions.', '2021-10-04', 4.15, '/assets/img/portfolio/hibernate.jpg', 0);
INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (22, 'Big-O Algorithm Analysis', 7, 'Data Structures are a specialized means of organizing and storing data in computers in such a way that we can perform operations on the stored data more efficiently. Data structures have a wide and diverse scope of usage across the fields of Computer Science and Software Engineering.', '2021-10-04', 4, '/assets/img/portfolio/big-O.jpg', 0);
INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (23, 'Web Development', 9, 'You’ll first build a website using HTML, style it with CSS, and then make it interactive using JavaScript. Along the way you’ll learn about the box model, which describes how websites are laid out by browsers, and about the Document Object Model, which your JavaScript code will interact with to dynamically change your web pages.', '2021-10-04', 4.3, '/assets/img/portfolio/web-dev.png', 0);
INSERT INTO virtual_teacher.courses (course_id, title, topic_id, description, starting_date, min_grade, picture_url, rating) VALUES (24, 'Python Data Structures ', 6, 'The core data structures of the Python programming language and learn how they are used. Designed as the next step up from the Programming for Everybody: Getting Started with Python course, this course moves past the basics of procedural programming.You’ll learn how to use the built-in data structures in Python, such as lists, dictionaries, and tuples, to perform more complex data analysis.', null, 4, '/assets/img/portfolio/python-data.jpeg', 0);


INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (7, 'OOP Overview and Classes', 'Understand what Object-Oriented Programming is. Explain classes and objects. Understand the benefits of OOP. Explain the difference between Class, Object, and Reference. Explain and use Enums.', 'https://www.youtube.com/embed/LYRDMfJ9OwA', 'Create your first Enum class and use it to calculate different shape area. ');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (8, 'Inheritance and Polymorphism', 'Understand and be able to use inheritance in your code. Learn a bit more about access modifiers and constructors. Be able to distinguish IS-A & HAS-A relationship. Understand and be able to use polymorphism in your code.', 'https://www.youtube.com/embed/I_PxbW716pA', 'Implements a simple calculator, which provides features for adding, subtracting, multiplying, and dividing numbers input by various means.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (9, 'Abstract Classes and Interfaces', 'Understand and be able to use abstract classes and methods in your code. Learn about interfaces. Be able to decide when to use interfaces and when abstract classes.
', 'https://www.youtube.com/embed/TIPnQe2-iPE', 'Create abstract class and multiple interfaces for the vehicle class we discussed in the lecture and create different vehicle types.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (10, 'Spring Overview', 'What is Spring and how it helps application development. Understand what are Java annotations. Get started with Spring framework. ', 'https://www.youtube.com/embed/UNINluR7se0', 'Describe how dependency injection and IOC works and what is AOP.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (11, 'REST and SPRING - GET', 'Create a simple REST controller.', 'https://www.youtube.com/embed/oRFCeRVWCNE', 'Create a simple REST controller.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (12, 'REST and SPRING - POST', 'Add POST methods and create DTO objects.', 'https://www.youtube.com/embed/aJbHK_RlcJg', 'Create 3 POST queries and 3 different DTOs.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (13, 'Databases Overview', 'Understand what a database server is. Understand what a Relational Database Management System (RDBMS) is.', 'https://www.youtube.com/embed/aurboE_q1Yg', 'Create your first database.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (14, 'Foreign Keys and Normalization', 'Understand what a primary key is. Understand what a foreign key is. Understand how to create relationships between tabled.', 'https://www.youtube.com/embed/_AMj02sANpI', 'Upgrade the table from your previous assignment, normalize it.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (15, 'Setting Up Database Server', 'Learn how to use IntelliJ to access and manage data on the database server.', 'https://www.youtube.com/embed/_R1hLus', 'Set up your first database server.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (16, 'Spring MVC + Thymeleaf Basics', 'Understand the two main web application architectures. Understand the MVC design pattern and how it fits in the layered architecture. Understand what is template engine.', 'https://www.youtube.com/embed/KTBWCJPKiqk', 'Create your first MVC Controller.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (17, 'Thymeleaf Advanced', 'Learn how to get input from a view (html form). Understand binding between View and object in the Model.', 'https://www.youtube.com/embed/SU0f3itiQPc', 'Create a website registration form.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (18, 'Using fragments and Validation', 'Learn to use Thymeleaf fragments to reuse front-end logic.
', 'https://www.youtube.com/embed/-ptZtMsg87U', 'Use fragments from the registration vie to create login.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (19, 'HTML & CSS Basics', 'Explain what the web is and what is the role of web clients and web servers. Demonstrate the browser developer tools and explain their purpose.', 'https://www.youtube.com/embed/UB1O30fR-EE', 'Create your first HTML page home page.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (20, 'Hibernate Basics', 'Explain what is ORM and how it helps software development. Understand the core components and terms in Hibernate. Understand what is transaction and why we need it.', 'https://www.youtube.com/embed/eFTnd4C0NWU', 'Implement data access using Hibernate: Configure Hibernate. Define persistent classes. Store and retrieve data. Define relationships.');
INSERT INTO virtual_teacher.lectures (lecture_id, title, description, video_url, assignment_description) VALUES (21, 'Algorithms Analysis & Linear Data Structures', 'Understand what is an algorithm. Understand algorithm complexity.', 'https://www.youtube.com/embed/8hly31xKli0', 'Submit a  form with the complexity of the tasks from the end of the lecture.');


INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (16, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (17, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (18, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (19, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (20, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (21, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (22, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (23, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (24, 25);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (16, 26);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (17, 26);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (18, 26);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (19, 26);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (20, 26);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (21, 26);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (22, 26);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (23, 26);
INSERT INTO virtual_teacher.course_teachers (course_id, user_id) VALUES (24, 26);


INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (16, 7);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (16, 8);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (16, 9);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (17, 10);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (17, 11);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (17, 12);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (18, 13);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (18, 14);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (18, 15);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (19, 16);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (19, 17);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (19, 18);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (20, 19);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (21, 20);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (22, 21);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (24, 21);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (23, 10);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (23, 16);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (23, 19);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (20, 18);
INSERT INTO virtual_teacher.course_lectures (course_id, lecture_id) VALUES (20, 16);
