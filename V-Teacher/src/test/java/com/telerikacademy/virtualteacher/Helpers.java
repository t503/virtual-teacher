package com.telerikacademy.virtualteacher;

import com.telerikacademy.virtualteacher.models.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("fake.email@gmail.com");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setPhoneNumber("0888888888");
        mockUser.setProfilePicture("mock-picture-location");
        mockUser.setAboutUser("mock-info-about-user");

        Set<Role> userRoles = new HashSet<>();
        userRoles.add(createMockCustomerRoll());
        mockUser.setUserRole(userRoles);

        Set<Course> userCourses = new HashSet<>();
        userCourses.add(createMockCourse());
        mockUser.setUserCourses(userCourses);

        return mockUser;
    }

    public static User createMockTeacher() {
        var mockTeacher = new User();
        mockTeacher.setId(1);
        mockTeacher.setEmail("fake.teacher.email@gmail.com");
        mockTeacher.setPassword("MockPassword");
        mockTeacher.setFirstName("MockTeacherFirstName");
        mockTeacher.setLastName("MockTeacherLastName");
        mockTeacher.setPhoneNumber("0888888887");
        mockTeacher.setProfilePicture("mock-teacher-picture-location");
        mockTeacher.setAboutUser("mock-info-about-teacher");

        Set<Role> userRoles = new HashSet<>();
        userRoles.add(createMockTeacherRoll());
        mockTeacher.setUserRole(userRoles);

        return mockTeacher;
    }

    public static User createMockAdmin() {
        var mockAdmin = new User();
        mockAdmin.setId(3);
        mockAdmin.setEmail("fake.admin.email@gmail.com");
        mockAdmin.setPassword("MockPassword");
        mockAdmin.setFirstName("MockAdminFirstName");
        mockAdmin.setLastName("MockAdminLastName");
        mockAdmin.setPhoneNumber("0888888889");
        mockAdmin.setProfilePicture("mock-admin-picture-location");
        mockAdmin.setAboutUser("mock-info-about-admin");

        Set<Role> userRoles = new HashSet<>();
        userRoles.add(createMockAdminRoll());
        mockAdmin.setUserRole(userRoles);

        return mockAdmin;
    }

    public static Role createMockCustomerRoll() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName("TestCustomerRole");
        return mockRole;
    }

    public static Role createMockTeacherRoll() {
        var mockRole = new Role();
        mockRole.setId(2);
        mockRole.setName("TestTeacherRole");
        return mockRole;
    }

    public static Role createMockAdminRoll() {
        var mockRole = new Role();
        mockRole.setId(3);
        mockRole.setName("TestAdminRole");
        return mockRole;
    }

    public static RoleRequest createMockRoleRequest() {
        var mockRoleRequest = new RoleRequest();
        mockRoleRequest.setId(1);
        mockRoleRequest.setRole(createMockTeacherRoll());
        mockRoleRequest.setUser(createMockUser());

        return mockRoleRequest;
    }

    public static Topic createMockTopic() {
        var mockTopic = new Topic();
        mockTopic.setId(1);
        mockTopic.setName("MockTopic");
        return mockTopic;
    }

    public static Lecture createMockLecture() {
        var mockLecture = new Lecture();
        mockLecture.setId(1);
        mockLecture.setTitle("MockLecture");
        mockLecture.setDescription("MockLectureDescription");
        mockLecture.setAssignmentDescription("MockAssignmentDescription");
        mockLecture.setVideoUrl("mock-video-URL");

        return mockLecture;
    }

    public static Assignment createMockAssignment() {
        var mockAssignment = new Assignment();
        mockAssignment.setId(1);
        mockAssignment.setUserGrade(5);
        mockAssignment.setLecture(createMockLecture());
        mockAssignment.setCourse(createMockCourse());
        mockAssignment.setUser(createMockUser());
        mockAssignment.setFilePath("mock-assignment-file-path");
        mockAssignment.setComplete(true);

        return mockAssignment;
    }

    public static Note createMockNote() {
        var mockNote = new Note();
        mockNote.setId(1);
        mockNote.setNote("MockNoteText");
        mockNote.setUserId(1);

        return mockNote;
    }

    public static Course createMockCourse() {
        var mockCourse = new Course();
        mockCourse.setId(1);
        mockCourse.setTitle("MockTitle");
        mockCourse.setDescription("MockCourseDescription");
        mockCourse.setTopic(createMockTopic());
        mockCourse.setPicture("mock-picture-location");
        mockCourse.setMingGrade(4);
        mockCourse.setStartingDate(LocalDate.of(2021, 9, 24));

        Set<User> teachersSet = new HashSet<>();
        teachersSet.add(createMockTeacher());
        mockCourse.setCourseTeachers(teachersSet);

        Set<Lecture> lectureSet = new HashSet<>();
        lectureSet.add(createMockLecture());
        mockCourse.setCourseLectures(lectureSet);

        return mockCourse;
    }

    public static EnrolledCourses createEnrolledCourse() {
        var mockEnrolledCourse = new EnrolledCourses();
        mockEnrolledCourse.setId(1);
        mockEnrolledCourse.setCourse(createMockCourse());
        mockEnrolledCourse.setUser(createMockUser());

        return mockEnrolledCourse;
    }
}
