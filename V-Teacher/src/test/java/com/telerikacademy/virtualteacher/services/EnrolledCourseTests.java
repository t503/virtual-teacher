package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.EnrolledCourses;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.CourseRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.EnrolledCoursesRepository;
import com.telerikacademy.virtualteacher.services.contracts.AssignmentService;
import com.telerikacademy.virtualteacher.services.impl.EnrolledCoursesServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.virtualteacher.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class EnrolledCourseTests {

    @Mock
    EnrolledCoursesRepository mockEnrolledCourseRepository;

    @Mock
    CourseRepository mockCourseRepository;

    @Mock
    AssignmentService mockAssignmentService;

    @InjectMocks
    EnrolledCoursesServiceImpl enrolledCoursesService;

    @Test
    public void update_Should_CallRepository_WhenUserTriesToUpdateEnrolledCourses() {
        var mockEnrolledCourse = createEnrolledCourse();

        enrolledCoursesService.update(mockEnrolledCourse);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .update(mockEnrolledCourse);
    }

    @Test
    public void rate_Should_CallRepository_WhenUserTriesToRateForTheFirstTime() {
        var mockEnrolledCourse = createEnrolledCourse();
        var mockCourse = createMockCourse();
        var mockUser = createMockUser();

        Mockito.when(mockEnrolledCourseRepository.getByUserAndCourse(Mockito.any(User.class), Mockito.any(Course.class))).thenReturn(mockEnrolledCourse);
        Mockito.doNothing().when(mockCourseRepository).update(mockCourse);

        enrolledCoursesService.rate(mockCourse, mockUser, 10);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .update(mockEnrolledCourse);
    }

    @Test
    public void rate_ShouldThrowEntityNotFoundException_WhenUserTriesToRateCourseThatIsNotYetRated() {
        var mockCourse = createMockCourse();
        var mockUser = createMockUser();

        Mockito.when(mockEnrolledCourseRepository.getByUserAndCourse(Mockito.any(User.class), Mockito.any(Course.class))).thenThrow(new EntityNotFoundException("message"));
        Mockito.doNothing().when(mockCourseRepository).update(mockCourse);

        enrolledCoursesService.rate(mockCourse, mockUser, 10);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .create(Mockito.any(EnrolledCourses.class));
    }

    @Test
    public void getByUserAndCourse_ShouldCallRepository_WhenUserTriesToGetByUserAndCourse() {
        var mockCourse = createMockCourse();
        var mockUser = createMockUser();

        enrolledCoursesService.getByUserAndCourse(mockUser, mockCourse);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .getByUserAndCourse(mockUser, mockCourse);
    }

    @Test
    public void getUserCompletedEnrolledCourses_ShouldCallRepository_WhenUserTriesToGetUserCompletedEnrolledCourses() {
        var mockUser = createMockUser();

        enrolledCoursesService.getUserCompletedEnrolledCourses(mockUser);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .getUserCompletedCourses(mockUser);
    }

    @Test
    public void getUserUncompletedEnrolledCourses_ShouldCallRepository_WhenUserTriesToGetUserUncompletedEnrolledCourses() {
        var mockUser = createMockUser();

        enrolledCoursesService.getUserCompletedEnrolledCourses(mockUser);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .getUserCompletedCourses(mockUser);
    }

    @Test
    public void getUserUncompletedEnrolledCourses_ShouldCallRepository_WhenUserTriesTogetUserUncompletedEnrolledCourses() {
        var mockUser = createMockUser();

        enrolledCoursesService.getUserUncompletedEnrolledCourses(mockUser);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .getUserEnrolledCourses(mockUser);
    }

    @Test
    public void deletedeleteEnrolledCourse_Should_CallRepositoryDelete_WhenAdminTriesToDeleteEnrolledCourse() {
        var mockEnrolledCourse = createEnrolledCourse();
        var mockAdmin = createMockAdmin();

        enrolledCoursesService.deleteEnrolledCourse(mockEnrolledCourse, mockAdmin);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .delete(mockEnrolledCourse.getId());
    }

    @Test
    public void deleteEnrolledCourse_Should_ThrowUnauthorizedOperationException_WhenUserTriesToCallIt() {
        var mockEnrolledCourse = createEnrolledCourse();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> enrolledCoursesService.deleteEnrolledCourse(mockEnrolledCourse, mockUser));
    }

    @Test
    public void getByCourse_Should_CallRepositorygetByCourse_WhenAdminToCallsIt() {
        var mockAdmin = createMockAdmin();
        var mockCourse = createMockCourse();

        enrolledCoursesService.getByCourse(mockCourse, mockAdmin);

        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
                .getByCourse(mockCourse);
    }

    @Test
    public void getByCourse_Should_ThrowUnauthorizedOperationException_WhenUserTriesToCallIt() {
        var mockCourse = createMockCourse();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> enrolledCoursesService.getByCourse(mockCourse, mockUser));
    }

//    @Test
//    public void areAllLecturesCompleted_ShouldCallRepository_WhenUserTriesTogetUserUncompletedEnrolledCourses() {
//        var mockUser = createMockUser();
//        var mockCourse = createMockCourse();
//        Set<Lecture> lectures = new HashSet<>();
//        lectures.add(createMockLecture());
//
//        Mockito.when(mockAssignmentService.getAllCompletedUserLectures(mockUser)).thenReturn((List<Lecture>) lectures);
//
//        enrolledCoursesService.areAllLecturesCompleted(mockCourse, mockUser);
//
//        Mockito.verify(mockEnrolledCourseRepository, Mockito.times(1))
//                .getUserEnrolledCourses(mockUser);
//    }



}
