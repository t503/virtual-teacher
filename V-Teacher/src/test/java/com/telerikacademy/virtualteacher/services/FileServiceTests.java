package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.repositories.contracts.FileRepository;
import com.telerikacademy.virtualteacher.services.impl.FileServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FileServiceTests {

    @Mock
    FileRepository mockFileRepository;

    @InjectMocks
    FileServiceImpl fileService;

    @Test
    public void getFile_Should_CallRepositoryGetFile() {
        fileService.getFile(Mockito.anyString());

        Mockito.verify(mockFileRepository, Mockito.times(1))
                .getFile(Mockito.anyString());
    }
}
