package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.repositories.contracts.LectureRepository;
import com.telerikacademy.virtualteacher.services.impl.LectureServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.virtualteacher.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class LectureServiceTests {

    @Mock
    LectureRepository mockLectureRepository;

    @InjectMocks
    LectureServiceImpl lectureService;


    @Test
    public void getAllLecture_Should_CallRepositoryGetAll() {
        lectureService.getAllLectures();

        Mockito.verify(mockLectureRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getLectureById_Should_CallRepositoryGetById() {
        lectureService.getLectureById(Mockito.anyInt());

        Mockito.verify(mockLectureRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getLectureByTitle_Should_CallRepositoryGetByField() {
        String title = "title";

        lectureService.getLectureByTitle(title);

        Mockito.verify(mockLectureRepository, Mockito.times(1))
                .getByField("title", title);
    }

    @Test
    public void createLecture_Should_ReturnLecture_WhenTeacherTriesToCreateLecture() {
        var mockTeacher = createMockTeacher();
        var mockLecture = createMockLecture();

        Mockito.when(mockLectureRepository.getByField("title",mockLecture.getTitle())).thenThrow(new EntityNotFoundException("message"));

        Mockito.when(mockLectureRepository.create(Mockito.any(Lecture.class)))
                .thenReturn(mockLecture);

        Lecture resultLecture = lectureService.createLecture(mockLecture, mockTeacher);

        Assertions.assertEquals(1, resultLecture.getId());
        Assertions.assertEquals(mockLecture.getTitle(), resultLecture.getTitle());
    }

    @Test
    public void createLecture_Should_ThrowDuplicateEntityException_WhenLectureWithSameTitleAlreadyExists() {
        var mockTeacher = createMockTeacher();
        var mockLecture = createMockLecture();

        Mockito.when(mockLectureRepository.getByField("title",mockLecture.getTitle())).thenReturn(mockLecture);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> lectureService.createLecture(mockLecture, mockTeacher));
    }

    @Test
    public void createLecture_Should_ThrowUnauthorizedOperationException_WhenUserWithRoleStudentTriesToCreateLecture() {
        var mockUser = createMockUser();
        var mockLecture = createMockLecture();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> lectureService.createLecture(mockLecture, mockUser));
    }

    @Test
    public void deleteLecture_Should_CallRepositoryWhenAdminTriesToDeleteLecture() {
        var mockLecture = createMockLecture();
        var mockAdmin = createMockAdmin();

        lectureService.deleteLecture(mockLecture.getId(), mockAdmin);

        Mockito.verify(mockLectureRepository, Mockito.times(1))
                .delete(Mockito.anyInt());
    }

    @Test
    public void deleteLecture_Should_ThrowUnauthorizedOperationExceptionWhenUserTriesToDeleteLecture() {
        var mockLecture = createMockLecture();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> lectureService.deleteLecture(mockLecture.getId(), mockUser));
    }

    @Test
    public void updateLecture_Should_CallRepository_WhenAdminTriesToUpdateAndLectureTitleIsBeingUpdated() {
        var mockAdmin = createMockAdmin();
        var mockLecture = createMockLecture();

        Mockito.when(mockLectureRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenThrow(new EntityNotFoundException("message"));

        lectureService.updateLecture(mockLecture, mockAdmin);

        Mockito.verify(mockLectureRepository, Mockito.times(1))
                .update(mockLecture);
    }

    @Test
    public void updateLecture_Should_CallRepository_WhenAdminTriesToUpdateAndLectureTitleMatchedLectureToBeUpdated() {
        var mockAdmin = createMockAdmin();
        var mockLecture = createMockLecture();

        Mockito.when(mockLectureRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenReturn(mockLecture);

        lectureService.updateLecture(mockLecture, mockAdmin);

        Mockito.verify(mockLectureRepository, Mockito.times(1))
                .update(mockLecture);
    }

    @Test
    public void updateLecture_Should_ThrowDuplicateEntityException_WhenDuplicateLectureTitleExists() {
        var mockAdmin = createMockAdmin();
        var mockLecture = createMockLecture();
        var mockLecture2 = createMockLecture();
        mockLecture.setTitle("DifferentMockTitle");

        Mockito.when(mockLectureRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenReturn(mockLecture2);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> lectureService.updateLecture(mockLecture, mockAdmin));
    }

    @Test
    public void updateLecture_Should_ThrowUnauthorizedOperationExceptionWhenUserTriesToUpdateLecture() {
        var mockLecture = createMockLecture();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> lectureService.updateLecture(mockLecture, mockUser));
    }

    @Test
    public void addNote_Should_CallRepositoryUpdate_WhenUserTriesToAddNote() {
        var mockUser = createMockUser();
        var mockLecture = createMockLecture();

        lectureService.addNote(mockLecture, mockUser);

        Mockito.verify(mockLectureRepository, Mockito.times(1))
                .update(mockLecture);
    }
}
