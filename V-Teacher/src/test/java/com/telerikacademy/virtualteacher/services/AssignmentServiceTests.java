package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.repositories.contracts.AssignmentRepository;
import com.telerikacademy.virtualteacher.services.impl.AssignmentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.virtualteacher.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class AssignmentServiceTests {

    @Mock
    AssignmentRepository mockAssignmentRepository;

    @InjectMocks
    AssignmentServiceImpl assignmentService;


    @Test
    public void getAllAssignments_Should_CallRepositoryGetAll() {
        var mockAdmin = createMockAdmin();

        assignmentService.getAll(mockAdmin);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAllAssignments_Should_ThrowUnauthorizedOperationException_WhenUserTriesToCallIt() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> assignmentService.getAll(mockUser));
    }

    @Test
    public void getAssignmentById_Should_CallRepositoryGetById() {
        var mockUser = createMockUser();
        var mockAssignment = createMockAssignment();

        Mockito.when(mockAssignmentRepository.getById(Mockito.anyInt())).thenReturn(mockAssignment);

        assignmentService.getById(Mockito.anyInt(), mockUser);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void createAssignment_Should_CallRepositoryCreate_WhenUserToCreateAssignment() {
        var mockUser = createMockUser();
        var mockAssignment = createMockAssignment();
        var mockCourse = createMockCourse();
        var mockLecture = createMockLecture();

        Mockito.when(mockAssignmentRepository.hasUserSubmitted(mockUser,mockCourse,mockLecture)).thenReturn(false);

        assignmentService.createAssignment(mockAssignment, mockUser, mockCourse,mockLecture);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .create(mockAssignment);
    }

    @Test
    public void createAssignment_Should_ThrowDuplicateEntityException_WhenUserToCreateAssignmentAfterItWasAlreadySubmitted() {
        var mockUser = createMockUser();
        var mockAssignment = createMockAssignment();
        var mockCourse = createMockCourse();
        var mockLecture = createMockLecture();

        Mockito.when(mockAssignmentRepository.hasUserSubmitted(mockUser,mockCourse,mockLecture)).thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> assignmentService.createAssignment(mockAssignment, mockUser, mockCourse, mockLecture));
    }

    @Test
    public void getAssignmentsPendingForGrading_Should_CallRepositoryGetAssignmentsPendingForGrading_WhenAdminTriesToCallIt() {
        var mockAdmin = createMockAdmin();

        assignmentService.getAssignmentsPendingForGrading(mockAdmin);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .getAssignmentsPendingForGrading();
    }

    @Test
    public void getAssignmentsPendingForGrading_Should_ThrowUnauthorizedOperationException_WhenUserTriesToCallIt() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> assignmentService.getAssignmentsPendingForGrading(mockUser));
    }

    @Test
    public void getAllCompletedUserLectures_Should_CallRepositoryGetAllCompletedUserLectures_WhenUserTriesToCallIt() {
        var mockUser = createMockUser();

        assignmentService.getAllCompletedUserLectures(mockUser, mockUser);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .getAllCompletedUserLectures(mockUser);
    }

    @Test
    public void getAllCompletedUserLectures_ShouldThrowUnauthorizedOperationException_WhenDifferentRequestUserTriesToCallIt() {
        var mockUser = createMockUser();
        var mockUser2 = createMockUser();
        mockUser2.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> assignmentService.getAllCompletedUserLectures(mockUser, mockUser2));
    }


    @Test
    public void gelAllAssignmentsForUser_Should_CallRepositoryGelAllAssignmentsForUser_WhenUserTriesToCallIt() {
        var mockUser = createMockUser();

        assignmentService.gelAllAssignmentsForUser(mockUser, mockUser);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .gelAllAssignmentsForUser(mockUser);
    }

    @Test
    public void gelAllAssignmentsForUser_Should_ThrowUnauthorizedOperationException_WhenUserTriesToGetOtherUsersAssignments() {
        var mockUser = createMockUser();
        var mockUser2 = createMockUser();
        mockUser2.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> assignmentService.gelAllAssignmentsForUser(mockUser, mockUser2));
    }

    @Test
    public void updateAssignment_Should_CallRepositoryUpdateAssignment_WhenAdminTriesToCallIt() {
        var mockAssignment = createMockAssignment();
        var mockAdmin = createMockAdmin();

        assignmentService.updateAssignment(mockAssignment, mockAdmin);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .update(mockAssignment);
    }

    @Test
    public void updateAssignment_Should_ThrowUnauthorizedOperationException_WhenUserTriesToCallIt() {
        var mockAssignment = createMockAssignment();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> assignmentService.updateAssignment(mockAssignment, mockUser));
    }

    @Test
    public void deleteAssignment_Should_CallRepositorydelete_WhenAdminTriesToDeleteAssignment() {
        var mockAssignment = createMockAssignment();
        var mockAdmin = createMockAdmin();

        assignmentService.deleteAssignment(mockAssignment, mockAdmin);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .delete(mockAssignment.getId());
    }

    @Test
    public void deleteAssignment_Should_ThrowUnauthorizedOperationException_WhenUserTriesToCallIt() {
        var mockAssignment = createMockAssignment();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> assignmentService.deleteAssignment(mockAssignment, mockUser));
    }

    @Test
    public void getAssignmentsByCourse_Should_CallRepositoryGetAssignmentsByCourse_WhenAdminToCallsIt() {
        var mockAdmin = createMockAdmin();
        var mockCourse = createMockCourse();

        assignmentService.getAssignmentsByCourse(mockCourse, mockAdmin);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .getAssignmentsByCourse(mockCourse);
    }

    @Test
    public void getAssignmentsByCourse_Should_ThrowUnauthorizedOperationException_WhenUserTriesToCallIt() {
        var mockCourse = createMockCourse();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> assignmentService.getAssignmentsByCourse(mockCourse, mockUser));
    }

    @Test
    public void getAssignment_Should_CallRepositoryGetAssignment_WhenUserTriesToCallIt() {
        var mockUser = createMockUser();
        var mockCourse = createMockCourse();
        var mockLecture = createMockLecture();

        assignmentService.getAssignment(mockUser, mockCourse, mockLecture);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .getAssignment(mockUser, mockCourse, mockLecture);
    }

    @Test
    public void getFilterAssignments_Should_CallRepositoryGetFilterAssignments_WhenUserTriesToCallIt() {
        String searchBy = "firstName";
        String value = "mockFirstName";

        assignmentService.getFilterAssignments(searchBy, value);

        Mockito.verify(mockAssignmentRepository, Mockito.times(1))
                .getFilteredAssignments(searchBy, value);
    }
}
