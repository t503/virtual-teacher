package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.RoleRequest;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.RoleRequestRepository;
import com.telerikacademy.virtualteacher.services.impl.RoleRequestServiceImpl;
import com.telerikacademy.virtualteacher.services.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import static com.telerikacademy.virtualteacher.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class RoleRequestTests {

    @Mock
    RoleRequestRepository mockRoleRequestRepository;

    @Mock
    UserServiceImpl userService;

    @InjectMocks
    RoleRequestServiceImpl roleRequestService;

    @Test
    public void getRoleRequestById_Should_CallRepositoryGetById() {
        roleRequestService.getRoleRequestById(Mockito.anyInt());

        Mockito.verify(mockRoleRequestRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getAllRoleRequests_Should_CallRepositoryGetAll() {
        var mockAdmin = createMockAdmin();

        roleRequestService.getAllRoleRequests(mockAdmin);

        Mockito.verify(mockRoleRequestRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAllRoleRequests_ShouldThrowUnauthorizedOperationException_WhenUserTriesToRequest() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> roleRequestService.getAllRoleRequests(mockUser));
    }

    @Test
    public void createRoleRequest_Should_CreateRoleRqeust_WhenAdminTriesToCreateRoleRequest() {
        var mockTeacher = createMockTeacher();
        var mockRoleRequest = createMockRoleRequest();
        var mockAdminRole = createMockAdminRoll();

        Mockito.when(mockRoleRequestRepository.getByUserAndRole(Mockito.any(User.class), Mockito.any(Role.class))).thenThrow(new EntityNotFoundException("message"));

        Mockito.when(mockRoleRequestRepository.create(Mockito.any(RoleRequest.class))).thenReturn(mockRoleRequest);

        RoleRequest resultRoleRequest = roleRequestService.createRoleRequest(mockRoleRequest, mockTeacher, mockAdminRole);

        Assertions.assertEquals(1, resultRoleRequest.getId());
    }

    @Test
    public void createRoleRequest_ShouldThrowDuplicateEntityException_WhenUserTriesToCreateRoleRequestWhenUserIsAlreadyWithThisRole() {
        var mockAdmin = createMockAdmin();
        var mockRoleRequest = createMockRoleRequest();
        var mockAdminRole = createMockAdminRoll();

        Mockito.when(mockRoleRequestRepository.getByUserAndRole(Mockito.any(User.class), Mockito.any(Role.class))).thenReturn(mockRoleRequest);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> roleRequestService.createRoleRequest(mockRoleRequest, mockAdmin, mockAdminRole));
    }

    @Test
    public void deleteRoleRequest_Should_CallRepositoryWhenAdminTriesToDeleteRoleRequest() {
        var mockAdmin = createMockAdmin();

        roleRequestService.deleteRoleRequest(mockAdmin, Mockito.anyInt());

        Mockito.verify(mockRoleRequestRepository, Mockito.times(1))
                .delete(Mockito.anyInt());
    }

    @Test
    public void deleteRoleRequest_ShouldThrowUnauthorizedOperationException_WhenUserTriesToDelete() {
        var mockAdmin = createMockAdmin();

        roleRequestService.deleteRoleRequest(mockAdmin, Mockito.anyInt());

        Mockito.verify(mockRoleRequestRepository, Mockito.times(1))
                .delete(Mockito.anyInt());
    }

    @Test
    public void approveRequest_Should_CallRepository_WhenAdminTriesToapproveRequestAndRequestIsValid() {
        var mockUser = createMockUser();
        var mockAdmin = createMockAdmin();
        var mockRoleRequest = createMockRoleRequest();

        roleRequestService.approveRequest(mockAdmin, mockRoleRequest);

        Mockito.verify(userService, Mockito.times(1))
                .updateUser(mockUser, mockAdmin);
    }
}
