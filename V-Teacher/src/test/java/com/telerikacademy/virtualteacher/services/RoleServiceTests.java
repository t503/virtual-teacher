package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.repositories.contracts.RoleRepository;
import com.telerikacademy.virtualteacher.services.impl.RoleServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class RoleServiceTests {

    @Mock
    RoleRepository mockRoleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Test
    public void getAllUsers_Should_CallRepositoryGetAll() {
        roleService.getAllRoles();

        Mockito.verify(mockRoleRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getRoleById_Should_CallRepositoryGetById() {
        roleService.getRoleById(Mockito.anyInt());

        Mockito.verify(mockRoleRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getRoleByName_Should_CallRepositoryGetByField() {
        String name = "firstName";

        roleService.getRoleByName(name);

        Mockito.verify(mockRoleRepository, Mockito.times(1))
                .getByField("name", name);
    }
}
