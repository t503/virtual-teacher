package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Note;
import com.telerikacademy.virtualteacher.repositories.contracts.NoteRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.UserRepository;
import com.telerikacademy.virtualteacher.services.impl.NoteServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.virtualteacher.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class NoteServiceTests {

    @Mock
    NoteRepository mockNoteRepository;

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    NoteServiceImpl noteService;

//    @Test
//    public void createNote_Should_ReturnNote_WhenTeacherTriesToCreateNote() {
//        var mockUser = createMockUser();
//        var mockLecture = createMockLecture();
//        var mockNote = createMockNote();
//
//        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(mockUser);
//
//        Note resultNote = noteService.createNote(mockNote, mockLecture);
//
//        Assertions.assertEquals(1, resultNote.getId());
//        Assertions.assertEquals(mockNote.getNote(), resultNote.getNote());
//    }
}
