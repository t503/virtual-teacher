package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.repositories.contracts.CourseRepository;
import com.telerikacademy.virtualteacher.services.impl.CourseServiceImpl;
import com.telerikacademy.virtualteacher.services.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.telerikacademy.virtualteacher.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CourseServiceTests {

    @Mock
    CourseRepository mockCourseRepository;

    @InjectMocks
    CourseServiceImpl courseService;

    @Mock
    UserServiceImpl userService;

    @Test
    public void getAllCourses_Should_CallRepositoryGetAll() {
        courseService.getAllCourses();

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getCoursesById_Should_CallRepositoryGetById() {
        courseService.getCourseById(Mockito.anyInt());

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getCoursesByTitle_Should_CallRepositoryGetByField() {
        String title = "title";

        courseService.getCourseByTitle(title);

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .getByField("title", title);
    }

    @Test
    public void createCourse_Should_ReturnCourse_WhenTeacherTriesToCreateCourse() {
        var mockTeacher = createMockTeacher();
        var mockCourse = createMockCourse();

        Mockito.when(mockCourseRepository.getByField("title",mockCourse.getTitle())).thenThrow(new EntityNotFoundException("message"));

        Mockito.when(mockCourseRepository.create(Mockito.any(Course.class)))
                .thenReturn(mockCourse);

        Course resultCourse = courseService.createCourse(mockCourse, mockTeacher);

        Assertions.assertEquals(1, resultCourse.getId());
        Assertions.assertEquals(mockCourse.getTitle(), resultCourse.getTitle());
    }

    @Test
    public void createCourse_Should_ThrowDuplicateEntityException_WhenCourseWithSameTitleAlreadyExists() {
        var mockTeacher = createMockTeacher();
        var mockCourse = createMockCourse();

        Mockito.when(mockCourseRepository.getByField("title",mockCourse.getTitle())).thenReturn(mockCourse);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> courseService.createCourse(mockCourse, mockTeacher));
    }

    @Test
    public void createCourse_Should_ThrowUnauthorizedOperationException_WhenUserWithRoleStudentTriesToCreateCourse() {
        var mockUser = createMockUser();
        var mockCourse = createMockCourse();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> courseService.createCourse(mockCourse, mockUser));
    }

    @Test
    public void deleteCourse_Should_CallRepositoryWhenAdminTriesToDeleteCourse() {
        var mockCourse = createMockCourse();
        var mockAdmin = createMockAdmin();

        courseService.deleteCourse(mockCourse.getId(), mockAdmin);

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .delete(Mockito.anyInt());
    }

    @Test
    public void deleteCourse_Should_ThrowUnauthorizedOperationExceptionWhenUserTriesToDeleteCourse() {
        var mockCourse = createMockCourse();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> courseService.deleteCourse(mockCourse.getId(), mockUser));
    }

    @Test
    public void updateCourse_Should_CallRepository_WhenAdminTriesToUpdateAndCourseTitleIsBeingUpdated() {
        var mockAdmin = createMockAdmin();
        var mockCourse = createMockCourse();

        Mockito.when(mockCourseRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenThrow(new EntityNotFoundException("message"));

        courseService.updateCourse(mockCourse, mockAdmin);

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .update(mockCourse);
    }

    @Test
    public void updateCourse_Should_CallRepository_WhenAdminTriesToUpdateAndCourseTitleMatchedCourseToBeUpdated() {
        var mockAdmin = createMockAdmin();
        var mockCourse = createMockCourse();

        Mockito.when(mockCourseRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenReturn(mockCourse);

        courseService.updateCourse(mockCourse, mockAdmin);

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .update(mockCourse);
    }

    @Test
    public void updateCourse_Should_ThrowDuplicateEntityException_WhenDuplicateCourseTitleExists() {
        var mockAdmin = createMockAdmin();
        var mockCourse = createMockCourse();
        var mockCourse2 = createMockCourse();
        mockCourse.setTitle("DifferentMockTitle");

        Mockito.when(mockCourseRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenReturn(mockCourse2);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> courseService.updateCourse(mockCourse, mockAdmin));
    }

    @Test
    public void updateCourse_Should_ThrowUnauthorizedOperationExceptionWhenUserTriesToUpdateCourse() {
        var mockCourse = createMockCourse();
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> courseService.updateCourse(mockCourse, mockUser));
    }

    @Test
    public void searchCourseByMVC_Should_CallRepository() {
        var mockCourse = createMockCourse();
        String searchBy = "title";
        String value = mockCourse.getTitle();

        courseService.searchCourseByMVC(searchBy, value);

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .getSearchedCourses(searchBy, value);
    }

    @Test
    public void sortedMvcCourses_Should_CallRepository() {
        var mockCourse = createMockCourse();
        String sortBy = "title";

        courseService.sortedMvcCourses(sortBy);

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .sortedMvcCourses(sortBy);
    }

    @Test
    public void sort_Should_CallRepository() {
        var mockCourse = createMockCourse();
        String sortBy = "title";

        courseService.sort(Optional.of(sortBy), Optional.empty());

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .sort(Optional.of(sortBy), Optional.empty());
    }

    @Test
    public void filter_Should_CallRepository() {
        var mockCourse = createMockCourse();
        String sortBy = "title";

        courseService.filter(Optional.of(sortBy), Optional.empty(), Optional.empty(), Optional.empty());

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .filter(Optional.of(sortBy), Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void getAllPublishedCourses_Should_CallRepository() {
        courseService.getAllPublishedCourses();

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .getAllPublishedCourses();
    }

    @Test
    public void addTeacher_Should_CallRepository_WhenAdminTriesToAddTeacher() {
        var mockAdmin = createMockAdmin();
        var mockTeacher = createMockTeacher();
        mockTeacher.setId(2);
        var mockCourse = createMockCourse();

        Mockito.when(userService.getUserById(mockTeacher.getId(), mockAdmin)).thenReturn(mockTeacher);
        Mockito.when(mockCourseRepository.getById(Mockito.anyInt())).thenReturn(mockCourse);

        courseService.addTeacher(mockTeacher.getId(), mockCourse.getId(), mockAdmin);

        Mockito.verify(mockCourseRepository, Mockito.times(1))
                .update(mockCourse);
    }

    @Test
    public void addTeacher_Should_ThrowDuplicateEntityException_WhenAdminTriesToAddTeacherThatIsAlreadyLeadingTheCourse() {
        var mockAdmin = createMockAdmin();
        var mockTeacher = createMockTeacher();
        var mockCourse = createMockCourse();

        Mockito.when(userService.getUserById(mockTeacher.getId(), mockAdmin)).thenReturn(mockTeacher);
        Mockito.when(mockCourseRepository.getById(Mockito.anyInt())).thenReturn(mockCourse);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> courseService.addTeacher(mockTeacher.getId(), mockCourse.getId(), mockAdmin));
    }

    @Test
    public void addTeacher_Should_ThrowUnauthorizedOperationException_WhenUserTriesToAddTeacher() {
        var mockUser = createMockUser();
        var mockTeacher = createMockTeacher();
        var mockCourse = createMockCourse();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> courseService.addTeacher(mockTeacher.getId(), mockCourse.getId(), mockUser));
    }

    @Test
    public void addTeacher_Should_ThrowUnauthorizedOperationException_WhenAdminTriesToAddTeacherThatIsWithRoleStudent() {
        var mockAdmin = createMockAdmin();
        var mockUser = createMockUser();
        var mockCourse = createMockCourse();

        Mockito.when(userService.getUserById(mockUser.getId(), mockAdmin)).thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> courseService.addTeacher(mockUser.getId(), mockCourse.getId(), mockAdmin));
    }
}
