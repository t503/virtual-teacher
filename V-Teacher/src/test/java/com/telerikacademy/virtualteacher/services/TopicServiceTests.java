package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.repositories.contracts.TopicRepository;
import com.telerikacademy.virtualteacher.services.contracts.TopicService;
import com.telerikacademy.virtualteacher.services.impl.TopicServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.virtualteacher.Helpers.createMockAdmin;

@ExtendWith(MockitoExtension.class)
public class TopicServiceTests {

    @Mock
    TopicRepository mockTopicRepository;

    @InjectMocks
    TopicServiceImpl topicService;


    @Test
    public void getAllTopics_Should_CallRepositoryGetAll() {

        topicService.getAllTopics();

        Mockito.verify(mockTopicRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getTopicById_Should_CallRepositoryGetById() {

        topicService.getTopicById(Mockito.anyInt());

        Mockito.verify(mockTopicRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getTopicByName_Should_CallRepositoryGetByField() {

        topicService.getTopicByName("mockTopicName");

        Mockito.verify(mockTopicRepository, Mockito.times(1))
                .getByField("name", "mockTopicName");
    }

}
