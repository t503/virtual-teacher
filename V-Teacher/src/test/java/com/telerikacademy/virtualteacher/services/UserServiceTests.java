package com.telerikacademy.virtualteacher.services;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.UserRepository;
import com.telerikacademy.virtualteacher.services.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.*;

import static com.telerikacademy.virtualteacher.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getUserByEmail_Should_ReturnUser_When_MatchExists() {
        var mockUser = createMockUser();

        Mockito.when(mockUserRepository.getUserByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        User result = userService.getByEmail(mockUser.getEmail());

        Assertions.assertEquals(mockUser.getId(), result.getId());
        Assertions.assertEquals(mockUser.getEmail(), result.getEmail());
        Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName());
        Assertions.assertEquals(mockUser.getLastName(), result.getLastName());
    }

    @Test
    public void getUserById_Should_ReturnUser_When_MatchExists() {
        var mockUser = createMockUser();

        Mockito.when(mockUserRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = userService.getUserById(mockUser.getId());

        Assertions.assertEquals(mockUser.getId(), result.getId());
        Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName());
        Assertions.assertEquals(mockUser.getLastName(), result.getLastName());
        Assertions.assertEquals(mockUser.getEmail(), result.getEmail());
    }

    @Test
    public void getUserByIdWithUserValidation_Should_ReturnUser_When_MatchExists() {
        var mockAdmin = createMockAdmin();

        Mockito.when(mockUserRepository.getById(mockAdmin.getId()))
                .thenReturn(mockAdmin);

        User result = userService.getUserById(mockAdmin.getId(), mockAdmin);

        Assertions.assertEquals(mockAdmin.getId(), result.getId());
        Assertions.assertEquals(mockAdmin.getFirstName(), result.getFirstName());
        Assertions.assertEquals(mockAdmin.getLastName(), result.getLastName());
        Assertions.assertEquals(mockAdmin.getEmail(), result.getEmail());
    }

    @Test
    public void getUserById_Should_ThrowUnauthorizedOperationException_WhenUserTriesToGetAnotherUserById() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.getUserById(mockUser.getId(), mockUser));
    }

    @Test
    public void createUser_Should_ReturnUser_WhenUserTriesToRegister() {
        var mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenThrow(new EntityNotFoundException("message"));

        Mockito.when(mockUserRepository.create(Mockito.any(User.class)))
                .thenReturn(mockUser);

        User resultUser = userService.createUser(mockUser);

        Assertions.assertEquals(1, resultUser.getId());
    }

    @Test
    public void createUser_Should_ThrowDuplicateEntityException_WhenUserWithSameEmailAlreadyRegistered() {
        var mockUser = createMockUser();

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.createUser(mockUser));
    }

    @Test
    public void deleteUser_Should_CallRepositoryWhenAdminTriesToDeleteUser() {
        var mockUser = createMockUser();
        var mockAdmin = createMockAdmin();

        userService.deleteUser(mockAdmin, mockUser.getId());

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .delete(Mockito.anyInt());
    }

    @Test
    public void deleteUser_Should_ThrowUnauthorizedOperationException_WhenUserTriesToDeleteAdmin() {
        var mockUser = createMockUser();
        var mockAdmin = createMockAdmin();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.deleteUser(mockUser, mockAdmin.getId()));
    }

    @Test
    public void updateUser_Should_CallRepository_WhenUserTriesToUpdate() {
        var mockUser = createMockUser();
        Mockito.when(mockUserRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenReturn(mockUser);

        userService.updateUser(mockUser, mockUser);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(Mockito.any(User.class));
    }

    @Test
    public void updateUser_Should_CallRepository_WhenAdminTriesToUpdate() {
        var mockAdmin = createMockAdmin();
        var mockUser = createMockUser();
        Mockito.when(mockUserRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenThrow(new EntityNotFoundException("message"));

        userService.updateUser(mockUser, mockAdmin);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(Mockito.any(User.class));
    }

    @Test
    public void updateUser_Should_ThrowDuplicateEntityException_WhenUserWithSameEmailAlreadyExists() {
        var mockUser = createMockUser();
        var mockUser2 = createMockUser();
        mockUser2.setEmail("mockUser2@mockmail.com");

        Mockito.when(mockUserRepository.getByField(Mockito.any(String.class), Mockito.any(String.class))).thenReturn(mockUser2);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.updateUser(mockUser, mockUser2));
    }

    @Test
    public void updateUser_Should_ThrowUnauthorizedOperationException_WhenUserTriesToUpdateAdminProfile() {
        var mockUser = createMockUser();
        var mockUser2 = createMockAdmin();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.updateUser(mockUser2, mockUser));
    }

    @Test
    public void getAllRoles_Should_CallRepositoryGetAll_When() {
        var mockAdmin = createMockAdmin();

        userService.getAllUsers(mockAdmin);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAllUsers_Should_TrowUnauthorizedOperationException_When_UserHasRoleStudent() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.getAllUsers(mockUser));
    }

    @Test
    public void getAllUsersCount_Should_CallRepositoryGetAll() {

        userService.getAllUsersCount();

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll();
    }


    @Test
    public void enrollCourse_Should_CallRepositoryUpdate_WhenUserTriesToEnrollCourse() {
        var mockUser = createMockUser();
        var mockCourse2 = createMockCourse();
        mockCourse2.setId(2);
        mockCourse2.setTitle("AnotherMockCourse");

        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(mockUser);

        userService.enrollCourse(mockUser, mockCourse2);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void enrollCourse_Should_ThrowUnauthorizedOperationException_WhenUserTriesToEnrollCourseThatHeAlreadyEnrolled() {
        var mockUser = createMockUser();
        var mockCourse = createMockCourse();

        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.enrollCourse(mockUser, mockCourse));
    }

    @Test
    public void enrollCourse_Should_ThrowUnauthorizedOperationException_WhenUserTriesToEnrollCourseThatHasNotBeenPublished() {
        var mockUser = createMockUser();
        var mockCourse2 = createMockCourse();
        mockCourse2.setId(2);
        mockCourse2.setTitle("AnotherMockCourse");
        mockCourse2.setStartingDate(null);

        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.enrollCourse(mockUser, mockCourse2));
    }

    @Test
    public void enrollCourse_Should_ThrowUnauthorizedOperationException_WhenUserTriesToEnrollCourseNeforeEnrollmentPeriodHasBegun() {
        var mockUser = createMockUser();
        var mockCourse2 = createMockCourse();
        mockCourse2.setId(2);
        mockCourse2.setTitle("AnotherMockCourse");
        mockCourse2.setStartingDate(LocalDate.of(2222, 12, 2));

        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.enrollCourse(mockUser, mockCourse2));
    }

    @Test
    public void getAllUnassignedTeachers_Should_CallRepositorygetAllTeachers() {
        var mockCourse = createMockCourse();

        userService.getAllUnassignedTeachers(mockCourse);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAllTeachers();
    }

    @Test
    public void searchUserByMVC_Should_CallRepository() {
        var mockAdmin = createMockAdmin();

        userService.searchUserByMVC(mockAdmin, "email", mockAdmin.getEmail());

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getSearchedUsers("email", mockAdmin.getEmail());
    }

    @Test
    public void searchUserByMVC_Should_ThrowUnauthorizedOperationExceptionWhenStudentTriesToSearch() {
        var mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.searchUserByMVC(mockUser, "email", mockUser.getEmail()));
    }

    @Test
    public void filter_Should_CallRepository() {
        var mockAdmin = createMockAdmin();

        userService.filter(Optional.of(mockAdmin.getEmail()), Optional.empty(), Optional.empty(), Optional.empty(), mockAdmin);

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .filter(Optional.of(mockAdmin.getEmail()), Optional.empty(), Optional.empty(), Optional.empty());
    }


    @Test
    public void getAllTeachers_Should_CallRepository() {
        userService.getAllTeachers();

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAllTeachers();
    }

    @Test
    public void searchUserBy_Should_CallRepositoryWhenSearchingByEmail() {

        userService.searchUserBy(Optional.of("userEmail"), Optional.empty());

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .searchByField("email", Optional.of("userEmail").get());
    }


    @Test
    public void searchUserBy_Should_CallRepositoryWhenSearchingByPhone() {

        userService.searchUserBy(Optional.empty(), Optional.of("userPhone"));

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .searchByField("phone_number", Optional.of("userPhone").get());
    }

    @Test
    public void searchUserBy_Should_CallRepositoryWhenSearchingByNeitherPhoneOrEmail() {

        userService.searchUserBy(Optional.empty(), Optional.empty());

        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll();
    }

}
