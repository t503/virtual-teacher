package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.models.Assignment;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.AssignmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AssignmentRepositoryImpl extends BaseModifyRepositoryImpl<Assignment> implements AssignmentRepository {

    @Autowired
    public AssignmentRepositoryImpl(SessionFactory sessionFactory) {
        super(Assignment.class, sessionFactory);
    }

    @Override
    public boolean hasUserSubmitted(User user, Course course, Lecture lecture) {
        try (Session session = sessionFactory.openSession()) {
            Query<Assignment> query = session.createQuery("from Assignment where user = :user and course = :course and lecture = :lecture", Assignment.class);
            query.setParameter("user", user);
            query.setParameter("course", course);
            query.setParameter("lecture", lecture);
            if (query.list().size() == 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Assignment getAssignment(User user, Course course, Lecture lecture) {
        try (Session session = sessionFactory.openSession()) {
            Query<Assignment> query = session.createQuery("from Assignment where user = :user and course = :course and lecture = :lecture", Assignment.class);
            query.setParameter("user", user);
            query.setParameter("course", course);
            query.setParameter("lecture", lecture);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Assignment", course.getTitle(), lecture.getTitle());
            }
            return query.list().get(0);
        }
    }

    @Override
    public List<Assignment> getAssignmentsByCourse(Course course) {
        try (Session session = sessionFactory.openSession()) {
            Query<Assignment> query = session.createQuery("from Assignment where course = :course", Assignment.class);
            query.setParameter("course", course);
            return query.list();
        }
    }

    @Override
    public List<Assignment> gelAllAssignmentsForUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Assignment> query = session.createQuery("from Assignment where user = :user", Assignment.class);
            query.setParameter("user", user);

            return query.list();
        }
    }

    @Override
    public List<Assignment> getAssignmentsPendingForGrading() {
        try (Session session = sessionFactory.openSession()) {
            Query<Assignment> query = session.createQuery("from Assignment where userGrade = 0", Assignment.class);
            return query.list();
        }
    }

    @Override
    public List<Assignment> getAllCompletedUserLectures(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Assignment> query = session.createQuery("from Assignment where user = :user and isComplete = true", Assignment.class);
            query.setParameter("user", user);
            return query.list();
        }
    }

    @Override
    public List<Assignment> getFilteredAssignments(String searchBy, String value) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Assignment where 1=1";

            try {
                if (searchBy.equals("User full name")) {
                    String[] arrValue = value.split(" ");
                    String firstName = arrValue[0];
                    String lastName = arrValue[1];
                    queryString += " AND user.firstName = :firstName and user.lastName = :lastName";
                    Query<Assignment> query = session.createQuery(queryString, Assignment.class);
                    query.setParameter("firstName", firstName);
                    query.setParameter("lastName", lastName);
                    if (query.list().size() == 0) {
                        return session.createQuery("from Assignment where 1=1", Assignment.class).list();
                    }
                    return query.list();
                }
            } catch (IndexOutOfBoundsException e) {
                return session.createQuery("from Assignment where 1=1", Assignment.class).list();
            }

            if (searchBy.equals("Course title")) {
                queryString += " AND course.title = :course";
                Query<Assignment> query = session.createQuery(queryString, Assignment.class);
                query.setParameter("course", value);
                if (query.list().size() == 0) {
                    return session.createQuery("from Assignment where 1=1", Assignment.class).list();
                }
                return query.list();
            }

            if (searchBy.equals("Lecture title")) {
                queryString += " AND lecture.title = :lecture";
                Query<Assignment> query = session.createQuery(queryString, Assignment.class);
                query.setParameter("lecture", value);
                if (query.list().size() == 0) {
                    return session.createQuery("from Assignment where 1=1", Assignment.class).list();
                }
                return query.list();
            }

            return session.createQuery(queryString, Assignment.class).list();
        }
    }
}
