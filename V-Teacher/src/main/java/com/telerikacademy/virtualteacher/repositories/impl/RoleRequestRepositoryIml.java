package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.models.EnrolledCourses;
import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.RoleRequest;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.RoleRequestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRequestRepositoryIml extends BaseModifyRepositoryImpl<RoleRequest> implements RoleRequestRepository {

    @Autowired
    public RoleRequestRepositoryIml(SessionFactory sessionFactory) {
        super(RoleRequest.class, sessionFactory);
    }

    public RoleRequest getByUserAndRole(User user, Role role){
        try (Session session = getSessionFactory().openSession()) {
            var query = session.createQuery("from RoleRequest " +
                    "where user = :user and role = :role", RoleRequest.class);
            query.setParameter("user", user);
            query.setParameter("role", role);
            var result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(
                        String.format("Role request with user %s and role %s not found", user.getId(), role.getId()));
            }
            return result.get(0);
        }
    }
}
