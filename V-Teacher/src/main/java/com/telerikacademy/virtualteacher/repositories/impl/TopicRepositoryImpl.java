package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.models.Topic;
import com.telerikacademy.virtualteacher.repositories.contracts.TopicRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TopicRepositoryImpl extends BaseGetRepositoryImpl<Topic> implements TopicRepository {

    @Autowired
    public TopicRepositoryImpl(SessionFactory sessionFactory) {
        super(Topic.class, sessionFactory);
    }
}
