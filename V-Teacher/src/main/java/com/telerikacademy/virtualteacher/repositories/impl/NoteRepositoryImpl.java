package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.models.Note;
import com.telerikacademy.virtualteacher.repositories.contracts.NoteRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NoteRepositoryImpl extends BaseModifyRepositoryImpl<Note> implements NoteRepository {

    @Autowired
    public NoteRepositoryImpl(SessionFactory sessionFactory) {
        super(Note.class, sessionFactory);
    }
}
