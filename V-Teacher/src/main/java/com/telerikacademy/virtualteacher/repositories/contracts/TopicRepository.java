package com.telerikacademy.virtualteacher.repositories.contracts;

import com.telerikacademy.virtualteacher.models.Topic;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseGetRepository;

public interface TopicRepository extends BaseGetRepository<Topic> {
}
