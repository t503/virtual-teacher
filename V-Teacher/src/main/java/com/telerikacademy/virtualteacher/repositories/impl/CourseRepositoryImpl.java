package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Topic;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.CourseRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.TopicRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CourseRepositoryImpl extends BaseModifyRepositoryImpl<Course> implements CourseRepository {

    private final UserRepository userRepository;
    private final TopicRepository topicRepository;

    @Autowired
    public CourseRepositoryImpl(SessionFactory sessionFactory, UserRepository userRepository, TopicRepository topicRepository) {
        super(Course.class, sessionFactory);
        this.userRepository = userRepository;
        this.topicRepository = topicRepository;
    }

    @Override
    public List<Course> getAllPublishedCourses(){
        try(Session session = sessionFactory.openSession()){
            Query<Course> query = session.createQuery("from Course where startingDate != null",Course.class);
            return query.list();
        }
    }

    @Override
    public List<Course> filter(Optional<String> title,
                               Optional<Integer> topicId,
                               Optional<Integer> teacherId,
                               Optional<Double> rating) {
        try (Session session = sessionFactory.openSession()) {

            String queryString = "from Course where 1=1";
            if (title.isPresent()) {
                queryString += " AND title = :title";
            }
            if (topicId.isPresent()) {
                queryString += " AND topic.id = :topicId";
            }
            if (teacherId.isPresent()) {
                queryString += " AND :user member of courseTeachers";
            }
            if (rating.isPresent()) {
                queryString += " AND rating = :rating";
            }
            Query<Course> query = session.createQuery(queryString, Course.class);
            title.ifPresent(s -> query.setParameter("title", s));
            topicId.ifPresent(integer -> query.setParameter("topicId", integer));
            if (teacherId.isPresent()){
                User user = userRepository.getById(teacherId.get());
                query.setParameter("user",user);
            }
            rating.ifPresent(aDouble -> query.setParameter("rating", aDouble));
            return query.list();
        }
    }

    @Override
    public List<Course> sort(Optional<String> title, Optional<String> rating) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Course where 1=1";
            if (title.isPresent()) {
                queryString += " ORDER BY title";
            }
            if (rating.isPresent()) {
                queryString += " ORDER BY rating DESC";
            }
            if (rating.isPresent() && title.isPresent()){
                throw new IllegalArgumentException("Courses can be sorted by one parameter at a time!");
            }
            Query<Course> query = session.createQuery(queryString, Course.class);
            return query.list();
        }
    }

    @Override
    public List<Course> sortedMvcCourses(String sortParam){
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Course where 1=1";
            if (sortParam.equals("Title")) {
                queryString += " ORDER BY title";
            }
            if (sortParam.equals("Rating")) {
                queryString += " ORDER BY rating DESC";
            }
            Query<Course> query = session.createQuery(queryString, Course.class);
            return query.list();
        }
    }

    @Override
    public List<Course> getSearchedCourses(String searchBy,String value){
        try(Session session = sessionFactory.openSession()){
            String queryString = "from Course c where 1=1";

            if (searchBy.equals("Title")) {
                queryString += " AND title like :title";
                Query<Course> query = session.createQuery(queryString, Course.class);
                query.setParameter("title", "%" + value + "%");
                return query.list();
            }

            try {
                if (searchBy.equals("Topic")) {
                    Topic topic = topicRepository.getByField("name",value);
                    queryString += " AND topic = :topic";
                    Query<Course> query = session.createQuery(queryString, Course.class);
                    query.setParameter("topic",topic);
                    return query.list();
                }
            } catch (EntityNotFoundException e) {
                return session.createQuery(queryString, Course.class).list();
            }

            if (searchBy.equals("Rating")){
                queryString += " AND rating >= :topic";
                Query<Course> query = session.createQuery(queryString, Course.class);
                query.setParameter("topic",Double.parseDouble(value));
                return query.list();
            }

            try {
                if (searchBy.equals("Teacher's email")){
                    User teacher = userRepository.getUserByEmail(value);
                    queryString += " AND :teacher member of courseTeachers";
                    Query<Course> query = session.createQuery(queryString, Course.class);
                    query.setParameter("teacher",teacher);
                    return query.list();
                }
            } catch (EntityNotFoundException e) {
                return session.createQuery(queryString, Course.class).list();
            }

            return session.createQuery(queryString, Course.class).list();
        }
    }


}
