package com.telerikacademy.virtualteacher.repositories.contracts;

import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseGetRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseModifyRepository;

import java.util.List;
import java.util.Optional;

public interface CourseRepository extends BaseGetRepository<Course>, BaseModifyRepository<Course> {

    List<Course> filter(Optional<String> title,
                        Optional<Integer> topicId,
                        Optional<Integer> teacherId,
                        Optional<Double> rating);

    List<Course> sort(Optional<String> title, Optional<String> rating);

    List<Course> getSearchedCourses(String searchBy,String value);

    List<Course> sortedMvcCourses(String sortParam);

    List<Course> getAllPublishedCourses();
}
