package com.telerikacademy.virtualteacher.repositories.contracts;

import com.telerikacademy.virtualteacher.models.Note;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseModifyRepository;

public interface NoteRepository extends BaseModifyRepository<Note> {
}
