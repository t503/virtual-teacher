package com.telerikacademy.virtualteacher.repositories.contracts;

import com.telerikacademy.virtualteacher.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseModifyRepository<User> {
    User getUserByEmail(String email);

    List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email, Optional<String> phoneNumber);

    List<User> getSearchedUsers(String searchBy,String value);

    List<User> getAllTeachers();
}
