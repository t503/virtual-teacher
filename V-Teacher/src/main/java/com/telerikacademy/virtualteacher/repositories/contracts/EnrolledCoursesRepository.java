package com.telerikacademy.virtualteacher.repositories.contracts;

import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.EnrolledCourses;
import com.telerikacademy.virtualteacher.models.User;

import java.util.List;

public interface EnrolledCoursesRepository extends BaseModifyRepository<EnrolledCourses> {
    EnrolledCourses getByUserAndCourse(User user, Course course);

    Double getAverage(Course course);

    List<EnrolledCourses> getUserEnrolledCourses(User user);

    List<EnrolledCourses> getUserCompletedCourses(User user);

    List<EnrolledCourses>  getByCourse(Course course);
}
