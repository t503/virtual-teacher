package com.telerikacademy.virtualteacher.repositories.contracts;

import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseGetRepository;

public interface RoleRepository extends BaseGetRepository<Role> {
}
