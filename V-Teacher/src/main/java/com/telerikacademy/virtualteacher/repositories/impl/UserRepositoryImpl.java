package com.telerikacademy.virtualteacher.repositories.impl;


import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.RoleRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {
    public static final int ROLE_TEACHER_ID = 2;
    private final RoleRepository roleRepository;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, RoleRepository roleRepository) {
        super(User.class, sessionFactory);
        this.roleRepository = roleRepository;
    }

    @Override
    public User getUserByEmail(String email){
        return getByField("email",email);
    }

    @Override
    public List<User> getAllTeachers(){
        try(Session session = sessionFactory.openSession()){
            Role teacherRole = roleRepository.getById(ROLE_TEACHER_ID);
            Query<User> query = session.createQuery("from User where :role member of userRole",User.class);
            query.setParameter("role",teacherRole);
            return query.list();
        }
    }

    @Override
    public List<User> getSearchedUsers(String searchBy,String value){
        try(Session session = sessionFactory.openSession()){
            String queryString = "from User where 1=1";

            if (searchBy.equals("Email")) {
                queryString += " AND email like :email";
                Query<User> query = session.createQuery(queryString, User.class);
                query.setParameter("email", "%" + value + "%");
                return query.list();
            }

            if (searchBy.equals("Phone number")) {
                queryString += " AND phoneNumber like :phone";
                Query<User> query = session.createQuery(queryString, User.class);
                query.setParameter("phone","%" + value + "%");
                return query.list();
            }

            if (searchBy.equals("First name")) {
                queryString += " AND firstName like :firstName";
                Query<User> query = session.createQuery(queryString, User.class);
                query.setParameter("firstName","%" + value + "%");
                return query.list();
            }

            if (searchBy.equals("Last name")) {
                queryString += " AND lastName like :lastName";
                Query<User> query = session.createQuery(queryString, User.class);
                query.setParameter("lastName","%" + value + "%");
                return query.list();
            }
            return session.createQuery(queryString, User.class).list();
        }
    }

    @Override
    public List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email, Optional<String> phoneNumber) {
        try (Session session = sessionFactory.openSession()) {

            String queryString = "from User where 1=1";
            if (firstName.isPresent()) {
                queryString += " AND firstName = :firstName";
            }
            if (lastName.isPresent()) {
                queryString += " AND lastName = :lastName";
            }
            if (email.isPresent()) {
                queryString += " AND email = :email";
            }
            if (phoneNumber.isPresent()) {
                queryString += " AND phoneNumber = :phoneNumber";
            }

            Query<User> query = session.createQuery(queryString, User.class);
            firstName.ifPresent(s -> query.setParameter("firstName", s));
            lastName.ifPresent(s -> query.setParameter("lastName", s));
            email.ifPresent(s -> query.setParameter("email", s));
            phoneNumber.ifPresent(s -> query.setParameter("phoneNumber", s));

            return query.list();
        }
    }


}
