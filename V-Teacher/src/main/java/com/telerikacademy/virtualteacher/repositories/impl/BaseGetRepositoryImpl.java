package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public abstract class BaseGetRepositoryImpl<T> implements BaseGetRepository<T> {

    private final Class<T> clazz;
    final SessionFactory sessionFactory;

    public BaseGetRepositoryImpl(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            var query = String.format("from %s ", clazz.getSimpleName());
            return session.createQuery(query, clazz).getResultList();
        }
    }

    @Override
    public T getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            var query = String.format("from %s where id = :id", clazz.getSimpleName());
            return session.createQuery(query, clazz)
                    .setParameter("id", id)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(clazz.getSimpleName(), id));
        }
    }

    @Override
    public T getByField(String fieldName, String fieldValue) {
        try (Session session = sessionFactory.openSession()) {
            var query = String.format("from %s where %s = :%s", clazz.getSimpleName(), fieldName, fieldName);
            return session.createQuery(query, clazz)
                    .setParameter(fieldName, fieldValue)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(clazz.getSimpleName(), fieldName, fieldValue));
        }
    }

    @Override
    public List<T> searchByField(String fieldName, String fieldValue) {
        try (Session session = sessionFactory.openSession()) {
            Query<T> query = session.createQuery(String.format("from %s where %s like :%s", clazz.getSimpleName(), fieldName, fieldName));
            query.setParameter(("" + fieldName), "%" + fieldValue + "%");

            if (query.list().isEmpty()){
                throw new EntityNotFoundException(clazz.getSimpleName(), fieldName, fieldValue);
            }
            return query.list();
        }
    }

}
