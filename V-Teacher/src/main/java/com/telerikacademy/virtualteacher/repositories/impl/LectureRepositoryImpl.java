package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.repositories.contracts.LectureRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LectureRepositoryImpl extends BaseModifyRepositoryImpl<Lecture> implements LectureRepository {

    @Autowired
    public LectureRepositoryImpl(SessionFactory sessionFactory) {
        super(Lecture.class, sessionFactory);
    }
}
