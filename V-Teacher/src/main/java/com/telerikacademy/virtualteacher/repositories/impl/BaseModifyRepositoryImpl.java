package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.repositories.contracts.BaseModifyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public abstract class BaseModifyRepositoryImpl<T> extends BaseGetRepositoryImpl<T> implements BaseModifyRepository<T> {

    public BaseModifyRepositoryImpl(Class<T> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
    }

    @Override
    public T create(T entity) {
        try(Session session = getSessionFactory().openSession()){
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        }
        return entity;
    }

    @Override
    public void update(T entity) {
        try(Session session = getSessionFactory().openSession()){
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        T toBeDeleted = getById(id);
        try(Session session = getSessionFactory().openSession()){
            session.beginTransaction();
            session.delete(toBeDeleted);
            session.getTransaction().commit();
        }
    }
}
