package com.telerikacademy.virtualteacher.repositories.contracts;

import com.telerikacademy.virtualteacher.models.Assignment;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.User;

import java.util.List;

public interface AssignmentRepository extends BaseModifyRepository<Assignment>{

    boolean hasUserSubmitted(User user, Course course, Lecture lecture);

    List<Assignment> getAssignmentsPendingForGrading();

    List<Assignment> getAllCompletedUserLectures(User user);

    Assignment getAssignment(User user, Course course, Lecture lecture);

    List<Assignment> gelAllAssignmentsForUser(User user);

    List<Assignment> getFilteredAssignments(String searchBy, String value);

    List<Assignment> getAssignmentsByCourse(Course course);

}
