package com.telerikacademy.virtualteacher.repositories.contracts;

public interface BaseModifyRepository<T> extends BaseGetRepository<T> {
    void delete(int id);

    void update(T entity);

    T create(T entity);

}
