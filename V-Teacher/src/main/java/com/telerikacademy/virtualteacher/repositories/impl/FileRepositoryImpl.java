package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.repositories.contracts.FileRepository;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

@Repository
public class FileRepositoryImpl implements FileRepository {

    private final Path root = Paths.get(".").normalize().toAbsolutePath();
    private final String RESOURCE_DIR = root.toString() + "/src/main/resources/static/uploads/";
    private final String OUTPUT_DIR = "/uploads/";

    @Override

    public String saveFile(MultipartFile file){
        String fileName = new Date().getTime() + "_" + file.getOriginalFilename();
        Path path = Paths.get(RESOURCE_DIR + fileName);
        try{
            Files.createDirectories(path.getParent());
            Files.write(path,file.getBytes());
        } catch (IOException e){
            e.printStackTrace();
        }
        return OUTPUT_DIR + fileName;
    }

    @Override
    public FileSystemResource getFile(String path){
        try{
            return new FileSystemResource(Paths.get(RESOURCE_DIR + path));
        }catch (Exception e){
            throw new RuntimeException();
        }
    }
}
