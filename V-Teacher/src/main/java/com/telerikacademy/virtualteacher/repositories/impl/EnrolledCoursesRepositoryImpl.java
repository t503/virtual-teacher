package com.telerikacademy.virtualteacher.repositories.impl;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.EnrolledCourses;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.EnrolledCoursesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Repository
public class EnrolledCoursesRepositoryImpl extends BaseModifyRepositoryImpl<EnrolledCourses> implements EnrolledCoursesRepository {
    @Autowired
    public EnrolledCoursesRepositoryImpl(SessionFactory sessionFactory) {
        super(EnrolledCourses.class, sessionFactory);
    }

    @Override
    public EnrolledCourses getByUserAndCourse(User user, Course course) {
        try (Session session = getSessionFactory().openSession()) {
            var query = session.createQuery("from EnrolledCourses " +
                    "where user = :user and course = :course", EnrolledCourses.class);
            query.setParameter("user", user);
            query.setParameter("course", course);
            var result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(
                        String.format("Rating with user %s and course %s not found",
                                user.getEmail(), course.getTitle()));
            }
            return result.get(0);
        }
    }

    @Override
    public List<EnrolledCourses>  getByCourse(Course course) {
        try (Session session = getSessionFactory().openSession()) {
            var query = session.createQuery("from EnrolledCourses " +
                    "where course = :course", EnrolledCourses.class);
            query.setParameter("course", course);
            return query.list();
        }
    }

    @Override
    public Double getAverage(Course course) {
        try (Session session = getSessionFactory().openSession()) {
            var query = session.createQuery("select avg(rating) " +
                    "from EnrolledCourses where course = :course", Double.class);
            query.setParameter("course", course);
            Double result = query.uniqueResult();
            return BigDecimal.valueOf(result).setScale(1, RoundingMode.HALF_UP).doubleValue();
        }
    }

    @Override
    public List<EnrolledCourses> getUserEnrolledCourses(User user){
        try(Session session = getSessionFactory().openSession()){
            var query = session.createQuery("from EnrolledCourses where user = :user and isComplete = false",EnrolledCourses.class);
            query.setParameter("user",user);
            return query.list();
        }
    }

    @Override
    public List<EnrolledCourses> getUserCompletedCourses(User user){
        try(Session session = sessionFactory.openSession()){
            var query = session.createQuery("from EnrolledCourses where user = :user and isComplete = true",EnrolledCourses.class);
            query.setParameter("user",user);
            return query.list();
        }
    }
}
