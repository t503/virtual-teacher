package com.telerikacademy.virtualteacher.repositories.contracts;


import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.RoleRequest;
import com.telerikacademy.virtualteacher.models.User;


public interface RoleRequestRepository extends BaseModifyRepository<RoleRequest> {

    RoleRequest getByUserAndRole(User user, Role role);
}
