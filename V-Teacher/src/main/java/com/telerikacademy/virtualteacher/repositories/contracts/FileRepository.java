package com.telerikacademy.virtualteacher.repositories.contracts;

import org.springframework.core.io.FileSystemResource;
import org.springframework.web.multipart.MultipartFile;

public interface FileRepository {

    String saveFile(MultipartFile multipartFile);

    FileSystemResource getFile(String path);
}
