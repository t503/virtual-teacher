package com.telerikacademy.virtualteacher.repositories.contracts;

import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseGetRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseModifyRepository;

public interface LectureRepository extends BaseGetRepository<Lecture>, BaseModifyRepository<Lecture> {
}
