package com.telerikacademy.virtualteacher.services.contracts;

import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.EnrolledCourses;
import com.telerikacademy.virtualteacher.models.User;

import java.util.List;

public interface EnrolledCoursesService {
    void rate(Course course, User user, double ratingValue);

    EnrolledCourses update(EnrolledCourses newCourse);

    Double getAverageRating(Course course);

    EnrolledCourses getByUserAndCourse(User user, Course course);

    boolean areAllLecturesCompleted(Course course, User user);

    boolean allCourseAssignmentsAreGraded(User user, Course course);

    double calculateCourseFinalGrade(User user, Course course);

    List<EnrolledCourses> getUserUncompletedEnrolledCourses(User user);

    List<EnrolledCourses> getUserCompletedEnrolledCourses(User user);

    List<EnrolledCourses> getByCourse(Course course,User user);

    public void deleteEnrolledCourse(EnrolledCourses enrolledCourses,User user);
}
