package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.*;
import com.telerikacademy.virtualteacher.repositories.contracts.CourseRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.EnrolledCoursesRepository;
import com.telerikacademy.virtualteacher.services.contracts.AssignmentService;
import com.telerikacademy.virtualteacher.services.contracts.EnrolledCoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EnrolledCoursesServiceImpl implements EnrolledCoursesService {
    public static final String YOU_MUST_BE_ENROLLED_FOR_THE_COURSE_YOU_WANT_TO_RATE_ERROR_MESSAGE = "You must be enrolled for the course you want to rate";
    public static final String ONLY_ADMINISTRATOR_CAN_ACSSES_THIS_SOURCE_ERROR_MESSAGE = "Only Administrator can acsses this source.";
    public static final String ONLY_ADMINISTRATORS_CAN_DELETE_ENROLLED_COURSES_ERROR_MESSAGE = "Only Administrators can delete enrolled courses.";

    private final EnrolledCoursesRepository enrolledCoursesRepository;
    private final CourseRepository courseRepository;
    private final AssignmentService assignmentService;

    @Autowired
    public EnrolledCoursesServiceImpl(EnrolledCoursesRepository repository, CourseRepository courseRepository,
                                      AssignmentService assignmentService) {
        this.enrolledCoursesRepository = repository;
        this.courseRepository = courseRepository;
        this.assignmentService = assignmentService;
    }

    @Override
    public EnrolledCourses update(EnrolledCourses enrolledCourse){
        enrolledCoursesRepository.update(enrolledCourse);
        return enrolledCourse;
    }

    @Override
    public void rate(Course course, User user, double ratingValue) {
        if (user.isOnlyStudent() || user.isTeacher() || user.isAdmin()) {
            if (user.getUserCourses().stream().anyMatch(cors -> cors.getId() == course.getId())) {

                try {
                    EnrolledCourses existingRecord = enrolledCoursesRepository.getByUserAndCourse(user, course);
                    existingRecord.setRating(ratingValue);
                    enrolledCoursesRepository.update(existingRecord);
                    course.setRating(getAverageRating(course));
                    courseRepository.update(course);
                } catch (EntityNotFoundException e) {
                    var rating = new EnrolledCourses(user, course, ratingValue);
                    enrolledCoursesRepository.create(rating);
                    course.setRating(getAverageRating(course));
                    courseRepository.update(course);
                }
            } else {
                throw new UnauthorizedOperationException(YOU_MUST_BE_ENROLLED_FOR_THE_COURSE_YOU_WANT_TO_RATE_ERROR_MESSAGE);
            }
        }
    }

    @Override
    public boolean areAllLecturesCompleted(Course course, User user) {
        Set<Course> userCourses = user.getUserCourses();
        if (userCourses.contains(course)) {
            boolean areAllLecturesCompleted = true;
            try {
                Set<Lecture> completedUserLectures = new HashSet<>(assignmentService.getAllCompletedUserLectures(user, user));
                Set<Lecture> courseLectures = course.getCourseLectures();

                    if(!completedUserLectures.containsAll(courseLectures)){
                        areAllLecturesCompleted = false;
                    }

            } catch (NullPointerException e) {
                areAllLecturesCompleted = false;
            }

            if (areAllLecturesCompleted) {
                EnrolledCourses enrolledCourses = getByUserAndCourse(user, course);
                enrolledCourses.setComplete(true);
                enrolledCoursesRepository.update(enrolledCourses);
            }
            return areAllLecturesCompleted;
        }
        return false;
    }

    @Override
    public boolean allCourseAssignmentsAreGraded(User user, Course course){

        EnrolledCourses enrolledCourse = enrolledCoursesRepository.getByUserAndCourse(user, course);
        Assignment lectureAssignment;
        for (Lecture enrolledLecture : enrolledCourse.getCourse().getCourseLectures()) {
            try {

                lectureAssignment = assignmentService.getAssignment(user, course, enrolledLecture);

                if (!lectureAssignment.hasGrade()){
                    return false;
                }

            }catch (EntityNotFoundException e){
                return false;
            }

        }
        return true;
    }

    @Override
    public double calculateCourseFinalGrade(User user, Course course) {
        if (allCourseAssignmentsAreGraded(user, course)) {

            EnrolledCourses enrolledCourse = enrolledCoursesRepository.getByUserAndCourse(user, course);
            Set<Lecture> enrolledLectures = enrolledCourse.getCourse().getCourseLectures();
            Assignment lectureAssignment;
            double gradeSum = 0;

            for (Lecture enrolledLecture : enrolledLectures) {
                lectureAssignment = assignmentService.getAssignment(user, course, enrolledLecture);
                gradeSum += lectureAssignment.getUserGrade();
            }

            double finalGrade = gradeSum / enrolledLectures.size();
            enrolledCourse.setFinalGrade(finalGrade);
            enrolledCoursesRepository.update(enrolledCourse);
            return finalGrade;
        }
        return 0.0;
    }

    @Override
    public EnrolledCourses getByUserAndCourse(User user, Course course) {
        return enrolledCoursesRepository.getByUserAndCourse(user, course);
    }

    @Override
    public List<EnrolledCourses> getUserCompletedEnrolledCourses(User user){
        return enrolledCoursesRepository.getUserCompletedCourses(user);
    }

    @Override
    public List<EnrolledCourses> getUserUncompletedEnrolledCourses(User user){
        return enrolledCoursesRepository.getUserEnrolledCourses(user);
    }

    @Override
    public List<EnrolledCourses> getByCourse(Course course,User user){
        if (user.isAdmin()){
            return enrolledCoursesRepository.getByCourse(course);
        }
        throw new UnauthorizedOperationException(ONLY_ADMINISTRATOR_CAN_ACSSES_THIS_SOURCE_ERROR_MESSAGE);
    }

    @Override
    public void deleteEnrolledCourse(EnrolledCourses enrolledCourses,User user){
        if (user.isAdmin()){
            enrolledCoursesRepository.delete(enrolledCourses.getId());
        } else if (!user.isAdmin()){
            throw new UnauthorizedOperationException(ONLY_ADMINISTRATORS_CAN_DELETE_ENROLLED_COURSES_ERROR_MESSAGE);
        }
    }

    @Override
    public Double getAverageRating(Course course) {
        return enrolledCoursesRepository.getAverage(course);
    }


}
