package com.telerikacademy.virtualteacher.services.contracts;

import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.User;

import java.util.List;
import java.util.Optional;

public interface CourseService {
    List<Course> getAllCourses();

    Course getCourseById(int id);

    Course getCourseByTitle(String name);

    Course createCourse(Course newCourse, User user);

    boolean deleteCourse(int id,User user);

    Course updateCourse(Course newCourse,User user);

    Course addTeacher(int teacherId,int courseId,User user);

    List<Course> filter(Optional<String> title,
                        Optional<Integer> topicId,
                        Optional<Integer> teacherId,
                        Optional<Double> rating);

    List<Course> sort(Optional<String> title,
                      Optional<String> rating);

    List<Course> searchCourseByMVC(String searchBy,String value);

    List<Course> sortedMvcCourses(String sortParam);

    List<Course> getAllPublishedCourses();

//    boolean areAllLecturesCompleted(Course course, User user);

//    String calculateCourseGrade(Course course);

//    boolean areAllLecturesCompleted(Course course);
}
