package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.*;
import com.telerikacademy.virtualteacher.repositories.contracts.AssignmentRepository;
import com.telerikacademy.virtualteacher.services.contracts.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    public static final String DUPLICATE_SUBMISSION_ERROR_MESSAGE = "User with ID %d already submitted assignment for lecture %s";
    public static final String UNAUTHORIZED_GRADING_ERROR_MESSAGE = "Only Teachers and Admins are authorized to grade assignments";
    public static final String ONLY_ADMINS_AND_TEACHERS_CAN_REQUEST_OTHER_USER_S_ASSIGNMENTS_ERROR_MESSAGE = "Only Admins and Teachers can request other user`s assignments";
    public static final String ONLY_TEACHERS_OR_ASSIGMENT_OWNERS_CAN_GET_ASSIGNMENT_ERROR_MESSAGE = "Only Teachers or Assigment owners can get assignment.";
    public static final String ONLY_TEACHERS_AND_ADMINS_CAN_VIEW_ALL_ASSIGNMENTS_ERROR_MESSAGE = "Only Teachers and Admins can view all Assignments";
    public static final String ONLY_TEACHERS_OR_THE_USERS_WHO_S_LECTURES_ARE_REQUESTING_CAN_VIEW_THE_LIST_ERROR_MESSAGE = "Only Teachers or the Users who`s lectures are requested can view the list";
    public static final String ONLY_TEACHERS_AND_ADMINS_CAN_UPDATE_ASSIGNMENTS_ERROR_MESSAGE = "Only Teachers and Admins can update assignments.";
    public static final String ONLY_ADMINISTRATOR_HAVE_ACSSES_TO_THIS_SOURCE_ERROR_MESSAGE = "Only Administrator have acsses to this source.";
    public static final String ONLY_ADMINISTRATOR_CAN_DELETE_ASSIGNMENT_ERROR_MESSAGE = "Only Administrator can delete assignment.";

    private final AssignmentRepository assignmentRepository;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository) {
        this.assignmentRepository = assignmentRepository;
    }

    @Override
    public List<Assignment> getAll(User user){
        if (user.isTeacher() || user.isAdmin()){
            return assignmentRepository.getAll();
        }
        throw new UnauthorizedOperationException(ONLY_TEACHERS_AND_ADMINS_CAN_VIEW_ALL_ASSIGNMENTS_ERROR_MESSAGE);
    }

    @Override
    public Assignment getById(int id, User user){
        Assignment assignment = assignmentRepository.getById(id);
        if(user.isTeacher() || user.isAdmin() || user.getId() == assignment.getUser().getId()) {
            return assignment;
        }
        throw new UnauthorizedOperationException(ONLY_TEACHERS_OR_ASSIGMENT_OWNERS_CAN_GET_ASSIGNMENT_ERROR_MESSAGE);
    }

    @Override
    public void createAssignment(Assignment assignment, User user,Course course, Lecture lecture){
        if (!hasUserSubmitted(user,course, lecture)){
            assignmentRepository.create(assignment);
        }else {
            throw new DuplicateEntityException(String.format(DUPLICATE_SUBMISSION_ERROR_MESSAGE,user.getId(),lecture.getId()));
        }
    }

    @Override
    public List<Assignment> getAssignmentsPendingForGrading(User user){
        if (user.isTeacher() || user.isAdmin()){
            return assignmentRepository.getAssignmentsPendingForGrading();
        }
        throw new UnauthorizedOperationException(UNAUTHORIZED_GRADING_ERROR_MESSAGE);
    }

    @Override
    public List<Lecture> getAllCompletedUserLectures(User user, User userRequesting){
        if (userRequesting.isAdmin() || userRequesting.isTeacher() || userRequesting.getId() == user.getId()){
            List<Assignment> submittedAssignments = assignmentRepository.getAllCompletedUserLectures(user);
            List<Lecture> completedLectures = new ArrayList<>();
            for (Assignment assignment:submittedAssignments) {
                completedLectures.add(assignment.getLecture());
            }
            return completedLectures;
        }
        throw new UnauthorizedOperationException(ONLY_TEACHERS_OR_THE_USERS_WHO_S_LECTURES_ARE_REQUESTING_CAN_VIEW_THE_LIST_ERROR_MESSAGE);
    }

    @Override
    public List<Assignment> gelAllAssignmentsForUser(User user, User userRequesting){
        if (userRequesting.isAdmin() || userRequesting.isTeacher() || user.getId() == userRequesting.getId()){
            return assignmentRepository.gelAllAssignmentsForUser(user);
        }
        throw new UnauthorizedOperationException(ONLY_ADMINS_AND_TEACHERS_CAN_REQUEST_OTHER_USER_S_ASSIGNMENTS_ERROR_MESSAGE);
    }

    @Override
    public Assignment updateAssignment(Assignment assignmentToUpdate, User user){
        if (user.isTeacher() || user.isAdmin()){
            assignmentRepository.update(assignmentToUpdate);
            return assignmentToUpdate;
        }
        throw new UnauthorizedOperationException(ONLY_TEACHERS_AND_ADMINS_CAN_UPDATE_ASSIGNMENTS_ERROR_MESSAGE);
    }

    @Override
    public void deleteAssignment(Assignment assignment,User user){
        if (user.isAdmin()){
            assignmentRepository.delete(assignment.getId());
        } else if (!user.isAdmin()){
            throw new UnauthorizedOperationException(ONLY_ADMINISTRATOR_CAN_DELETE_ASSIGNMENT_ERROR_MESSAGE);
        }
    }

    @Override
    public List<Assignment> getAssignmentsByCourse(Course course,User user){
        if (user.isAdmin()){
            return assignmentRepository.getAssignmentsByCourse(course);
        }
        throw new UnauthorizedOperationException(ONLY_ADMINISTRATOR_HAVE_ACSSES_TO_THIS_SOURCE_ERROR_MESSAGE);
    }

    @Override
    public boolean hasUserSubmitted(User user,Course course ,Lecture lecture){
       return assignmentRepository.hasUserSubmitted(user,course, lecture);
    }

    @Override
    public Assignment getAssignment(User user, Course course, Lecture lecture){
        return assignmentRepository.getAssignment(user,course,lecture);
    }

    @Override
    public List<Assignment> getFilterAssignments(String searchBy, String value){
        return assignmentRepository.getFilteredAssignments(searchBy, value);
    }
}
