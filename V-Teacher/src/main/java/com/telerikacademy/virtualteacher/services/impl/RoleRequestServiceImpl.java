package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.RoleRequest;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.RoleRequestRepository;
import com.telerikacademy.virtualteacher.services.contracts.RoleRequestService;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class RoleRequestServiceImpl implements RoleRequestService {
    public static final String ONLY_USER_WITH_ROLE_ADMINISTRATOR_CAN_GET_ALL_ROLE_REQUESTS_ERROR_MESSAGE = "Only user with role Administrator can get all role requests.";
    public static final String USER_ALREADY_REQUESTED_THAT_ROLE_ERROR_MESSAGE = "User already requested that role";
    public static final String ONLY_USERS_WITH_ROLE_ADMINISTRATOR_CAN_DELETE_ROLE_REQUESTS_ERROR_MESSAGE = "Only users with role Administrator can delete role requests.";

    private final RoleRequestRepository roleRequestRepository;
    private final UserService userService;

    @Autowired
    public RoleRequestServiceImpl(RoleRequestRepository roleRequestRepository, UserService userService) {
        this.roleRequestRepository = roleRequestRepository;
        this.userService = userService;
    }

    @Override
    public RoleRequest getRoleRequestById(int id) {
        return roleRequestRepository.getById(id);
    }

    @Override
    public List<RoleRequest> getAllRoleRequests(User user) {
        if (user.isAdmin()) {
            return roleRequestRepository.getAll();
        }
        throw new UnauthorizedOperationException(ONLY_USER_WITH_ROLE_ADMINISTRATOR_CAN_GET_ALL_ROLE_REQUESTS_ERROR_MESSAGE);
    }


    @Override
    public RoleRequest createRoleRequest(RoleRequest roleRequest, User user, Role role) {
        boolean duplicateUserExists = true;

        try {
            roleRequestRepository.getByUserAndRole(user, role);
        } catch (EntityNotFoundException e) {
            duplicateUserExists = false;
        }

        if (duplicateUserExists) {
            throw new DuplicateEntityException(USER_ALREADY_REQUESTED_THAT_ROLE_ERROR_MESSAGE);
        }
        return roleRequestRepository.create(roleRequest);
    }

    @Override
    public boolean deleteRoleRequest(User user, int id) {
        if (user.isAdmin()) {
            roleRequestRepository.delete(id);
            return true;
        }
        throw new UnauthorizedOperationException(ONLY_USERS_WITH_ROLE_ADMINISTRATOR_CAN_DELETE_ROLE_REQUESTS_ERROR_MESSAGE);
    }

    @Override
    public void approveRequest(User user, RoleRequest roleRequest) {
        if (user.isAdmin()) {
            User userToApprove = roleRequest.getUser();
            Role roleToSet = roleRequest.getRole();
            Set<Role> userRoles = userToApprove.getUserRole();
            userRoles.add(roleToSet);
            userService.updateUser(userToApprove,user);
            deleteRoleRequest(user,roleRequest.getId());
        }
    }

}
