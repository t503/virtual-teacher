package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.UserRepository;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    public static final String ONLY_USER_WITH_ROLE_TEACHER_AND_ADMINISTRATOR_CAN_GET_ALL_USERS_ERROR_MESSAGE = "Only user with role Teacher and Administrator can get all users.";
    public static final String ONLY_USERS_WITH_ROLE_ADMINISTRATOR_CAN_DELETE_USERS_ERROR_MESSAGE = "Only users with role Administrator can delete users.";
    public static final String USERS_CAN_UPDATE_ONLY_THEIR_PROFILE_ONLY_USER_WITH_ROLE_ADMINISTRATOR_CAN_UPDATE_ANY_USER_ERROR_MESSAGE = "Users can update only their profile.Only user with role Administrator can update any user.";
    public static final String YOU_HAVE_ALREADY_ENROLLED_FOR_THIS_COURSE_ERROR_MESSAGE = "You have already enrolled for this course";
    public static final String COURSE_HAS_NOT_BEEN_PUBLISHED_YET_ERROR_MESSAGE = "Course has not been published yet";
    public static final String COURSES_CAN_BE_ENROLLED_AFTER_THE_ENROLLMENT_PERIOD_HAS_BEGUN_ERROR_MESSAGE = "Courses can be enrolled after the enrollment period has begun";
    public static final String STUDENT_CANNOT_SEE_AND_SEARCH_FOR_OTHER_STUDENTS_ERROR_MESSAGE = "Student cannot see and search for other students";
    public static final String ONLY_ADMINS_AND_TEACHERS_CAN_VIEW_OTHER_USER_S_PROFILES_ERROR_MESSAGE = "Only Admins and Teachers can view other user`s profiles.";

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers(User user) {
        if (user.isTeacher() || user.isAdmin()) {
            return userRepository.getAll();
        }
        throw new UnauthorizedOperationException(ONLY_USER_WITH_ROLE_TEACHER_AND_ADMINISTRATOR_CAN_GET_ALL_USERS_ERROR_MESSAGE);
    }

    @Override
    public int getAllUsersCount() {
        return userRepository.getAll().size();
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public User getUserById(int id, User user) {
        if (user.isAdmin() || user.isTeacher()){
            return userRepository.getById(id);
        }
        throw new UnauthorizedOperationException(ONLY_ADMINS_AND_TEACHERS_CAN_VIEW_OTHER_USER_S_PROFILES_ERROR_MESSAGE);
    }

    @Override
    public User getUserById(int id) {
            return userRepository.getById(id);
    }

    @Override
    public User createUser(User newUser) {
        boolean duplicateUserExists = true;

        try {
            userRepository.getByField("email", newUser.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateUserExists = false;
        }

        if (duplicateUserExists) {
            throw new DuplicateEntityException("User", "email", newUser.getEmail());
        }
        return userRepository.create(newUser);
    }

    @Override
    public boolean deleteUser(User user, int id) {
        if (user.isAdmin()) {
            userRepository.delete(id);
            return true;
        }
        throw new UnauthorizedOperationException(ONLY_USERS_WITH_ROLE_ADMINISTRATOR_CAN_DELETE_USERS_ERROR_MESSAGE);
    }

    @Override
    public User updateUser(User userToUpdate, User userUpdating) {
        if (userToUpdate.getId() == userUpdating.getId() || userUpdating.isAdmin()) {
            boolean duplicateUserExist = true;

            try {
                User user = userRepository.getByField("email", userToUpdate.getEmail());
                if (userToUpdate.getEmail().equalsIgnoreCase(user.getEmail())) {
                    duplicateUserExist = false;
                }
            } catch (EntityNotFoundException e) {
                duplicateUserExist = false;
            }

            if (duplicateUserExist) {
                throw new DuplicateEntityException("User", "email", userToUpdate.getEmail());
            }

            userRepository.update(userToUpdate);
            return userToUpdate;
        }
        throw new UnauthorizedOperationException(USERS_CAN_UPDATE_ONLY_THEIR_PROFILE_ONLY_USER_WITH_ROLE_ADMINISTRATOR_CAN_UPDATE_ANY_USER_ERROR_MESSAGE);
    }

    @Override
    public User enrollCourse(User userToUpdate, Course course) {
        for (Course userCourse : userRepository.getById(userToUpdate.getId()).getUserCourses()) {
            if (userCourse.getId() == course.getId()) {
                throw new UnauthorizedOperationException(YOU_HAVE_ALREADY_ENROLLED_FOR_THIS_COURSE_ERROR_MESSAGE);
            }
        }
        try {
            if (course.getStartingDate().isBefore(LocalDate.now()) || course.getStartingDate().equals(LocalDate.now())) {
                userRepository.update(userToUpdate);
                return userToUpdate;
            }
        } catch (NullPointerException e) {
            throw new UnauthorizedOperationException(COURSE_HAS_NOT_BEEN_PUBLISHED_YET_ERROR_MESSAGE);
        }
        throw new UnauthorizedOperationException(COURSES_CAN_BE_ENROLLED_AFTER_THE_ENROLLMENT_PERIOD_HAS_BEGUN_ERROR_MESSAGE);
    }

    @Override
    public List<User> searchUserBy(Optional<String> email, Optional<String> phone) {
        if (email.isPresent()) {
            return userRepository.searchByField("email", email.get());
        }
        if (phone.isPresent()) {
            return userRepository.searchByField("phone_number", phone.get());
        }
        return userRepository.getAll();
    }

    @Override
    public List<User> getAllTeachers() {
        return userRepository.getAllTeachers();
    }

    @Override
    public List<User> getAllUnassignedTeachers(Course course) {
        List<User> allTeachers = getAllTeachers();
        Set<User> courseTeachers = course.getCourseTeachers();
        List<User> allUnassignedTeachers = new ArrayList<>(allTeachers);
        allUnassignedTeachers.removeAll(courseTeachers);
        return allUnassignedTeachers;
    }

    @Override
    public List<User> searchUserByMVC(User user, String searchBy, String value) {
        if (user.isTeacher() || user.isAdmin()) {
            return userRepository.getSearchedUsers(searchBy, value);
        }
        throw new UnauthorizedOperationException(STUDENT_CANNOT_SEE_AND_SEARCH_FOR_OTHER_STUDENTS_ERROR_MESSAGE);
    }

    @Override
    public List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email, Optional<String> phoneNumber, User user) {
        return userRepository.filter(firstName, lastName, email, phoneNumber);
    }
}
