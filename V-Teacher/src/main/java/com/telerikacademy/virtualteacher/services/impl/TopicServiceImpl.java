package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.models.Topic;
import com.telerikacademy.virtualteacher.repositories.contracts.TopicRepository;
import com.telerikacademy.virtualteacher.services.contracts.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {

    private final TopicRepository topicRepository;

    @Autowired
    public TopicServiceImpl(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    @Override
    public List<Topic> getAllTopics(){
        return topicRepository.getAll();
    }

    @Override
    public Topic getTopicById(int id){
        return topicRepository.getById(id);
    }

    @Override
    public Topic getTopicByName(String name){
        return topicRepository.getByField("name", name);
    }

}
