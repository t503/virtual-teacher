package com.telerikacademy.virtualteacher.services.contracts;

import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.RoleRequest;
import com.telerikacademy.virtualteacher.models.User;

import java.util.List;

public interface RoleRequestService {
    RoleRequest getRoleRequestById(int id);

    List<RoleRequest> getAllRoleRequests(User user);

    RoleRequest createRoleRequest(RoleRequest roleRequest, User user, Role role);

    boolean deleteRoleRequest(User user, int id);

    void approveRequest(User user, RoleRequest roleRequest);
}
