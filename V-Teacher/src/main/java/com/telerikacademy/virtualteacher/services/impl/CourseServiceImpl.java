package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.CourseRepository;
import com.telerikacademy.virtualteacher.services.contracts.CourseService;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CourseServiceImpl implements CourseService {

    public static final String ONLY_TEACHER_OR_ADMINISTRATOR_CAN_CREATE_COURSES_ERROR_MESSAGE = "Only users with role Teacher or Administrator can create courses";
    public static final String ONLY_USERS_WITH_ROLE_ADMINISTRATOR_CAN_DELETE_COURSES_ERROR_MESSAGE = "Only users with role Administrator can delete courses";
    public static final String ONLY_USERS_WITH_ROLE_TEACHER_OR_ADMINISTRATORS_CAN_UPDATE_COURSES_ERROR_MESSAGE = "Only users with role Teacher or Administrators can update courses";
    public static final String USER_ALREADY_SET_AS_TEACHER_IN_THIS_COURSE_ERROR_MESSAGE = "User already set as teacher in this course.";
    public static final String ONLY_USERS_WITH_ROLE_TEACHER_CAN_BE_ADDED_TO_TEACH_IN_A_COURSE_ERROR_MESSAGE = "Only users with role Teacher can be added to teach in a course.";
    public static final String ONLY_USER_WITH_ROLE_TEACHER_OR_ADMINISTRATOR_CAN_ADD_TEACHERS_TO_COURSE_ERROR_MESSAGE = "Only user with role Teacher or Administrator can add teachers to course.";
    private final CourseRepository courseRepository;
    private final UserService userService;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, UserService userService) {
        this.courseRepository = courseRepository;
        this.userService = userService;
    }

    @Override
    public List<Course> getAllCourses() {
        return courseRepository.getAll();
    }

    @Override
    public Course getCourseById(int id) {
        return courseRepository.getById(id);
    }

    @Override
    public Course getCourseByTitle(String title) {
        return courseRepository.getByField("title", title);
    }

    @Override
    public Course createCourse(Course newCourse, User user) {
        if (user.isTeacher() || user.isAdmin()) {
            boolean duplicateCourseExists = true;

            try {
                courseRepository.getByField("title", newCourse.getTitle());
            } catch (EntityNotFoundException e) {
                duplicateCourseExists = false;
            }

            if (duplicateCourseExists) {
                throw new DuplicateEntityException("Course", "title", newCourse.getTitle());
            }
            return courseRepository.create(newCourse);
        }
        throw new UnauthorizedOperationException(ONLY_TEACHER_OR_ADMINISTRATOR_CAN_CREATE_COURSES_ERROR_MESSAGE);
    }

    @Override
    public boolean deleteCourse(int id, User user) {
        if (user.isAdmin()) {
            courseRepository.delete(id);
            return true;
        }
        throw new UnauthorizedOperationException(ONLY_USERS_WITH_ROLE_ADMINISTRATOR_CAN_DELETE_COURSES_ERROR_MESSAGE);
    }

    @Override
    public Course updateCourse(Course courseToUpdate, User user) {
        if (user.isTeacher() || user.isAdmin()) {
            boolean duplicateCourseExists = true;

            try {
                Course course = courseRepository.getByField("title", courseToUpdate.getTitle());
                if (courseToUpdate.getTitle().equalsIgnoreCase(course.getTitle())) {
                    duplicateCourseExists = false;
                }
            } catch (EntityNotFoundException e) {
                duplicateCourseExists = false;
            }

            if (duplicateCourseExists) {
                throw new DuplicateEntityException("Course", "title", courseToUpdate.getTitle());
            }
            courseRepository.update(courseToUpdate);
            return courseToUpdate;
        }
        throw new UnauthorizedOperationException(ONLY_USERS_WITH_ROLE_TEACHER_OR_ADMINISTRATORS_CAN_UPDATE_COURSES_ERROR_MESSAGE);
    }

    @Override
    public List<Course> getAllPublishedCourses(){
        return courseRepository.getAllPublishedCourses();
    }


    @Override
    public Course addTeacher(int teacherId, int courseId, User user) {
        if (user.isTeacher() || user.isAdmin()) {
            User teacher = userService.getUserById(teacherId, user);
            if (teacher.isTeacher()) {
                Course courseToAddTeacher = getCourseById(courseId);
                if (courseToAddTeacher.getCourseTeachers().stream().anyMatch(userTeach -> userTeach.getId() == teacher.getId())) {
                    throw new DuplicateEntityException(USER_ALREADY_SET_AS_TEACHER_IN_THIS_COURSE_ERROR_MESSAGE);
                } else {
                    Set<User> courseTeachers = courseToAddTeacher.getCourseTeachers();
                    courseTeachers.add(teacher);
                    courseRepository.update(courseToAddTeacher);
                    return courseToAddTeacher;
                }
            } else {
                throw new UnauthorizedOperationException(ONLY_USERS_WITH_ROLE_TEACHER_CAN_BE_ADDED_TO_TEACH_IN_A_COURSE_ERROR_MESSAGE);
            }
        }
        throw new UnauthorizedOperationException(ONLY_USER_WITH_ROLE_TEACHER_OR_ADMINISTRATOR_CAN_ADD_TEACHERS_TO_COURSE_ERROR_MESSAGE);
    }

    @Override
    public List<Course> filter(Optional<String> title,
                               Optional<Integer> topicId,
                               Optional<Integer> teacherId,
                               Optional<Double> rating) {
        return courseRepository.filter(title, topicId, teacherId, rating);
    }

    @Override
    public List<Course> sort(Optional<String> title, Optional<String> rating) {
        return courseRepository.sort(title, rating);
    }

    @Override
    public List<Course> sortedMvcCourses(String sortParam){
        return courseRepository.sortedMvcCourses(sortParam);
    }

    @Override
    public List<Course> searchCourseByMVC(String searchBy,String value){
        return courseRepository.getSearchedCourses(searchBy, value);
    }
}
