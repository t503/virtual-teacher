package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.repositories.contracts.FileRepository;
import com.telerikacademy.virtualteacher.services.contracts.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;

    @Autowired
    public FileServiceImpl(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    @Override
    public String saveFile(MultipartFile multipartFile) {
        return fileRepository.saveFile(multipartFile);
    }

    @Override
    public FileSystemResource getFile(String path) {
        return fileRepository.getFile(path);
    }
}
