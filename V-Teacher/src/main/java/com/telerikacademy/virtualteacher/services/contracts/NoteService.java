package com.telerikacademy.virtualteacher.services.contracts;

import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.Note;
import com.telerikacademy.virtualteacher.repositories.contracts.BaseModifyRepository;

public interface NoteService  {

    Note createNote(Note note, Lecture lecture);
}
