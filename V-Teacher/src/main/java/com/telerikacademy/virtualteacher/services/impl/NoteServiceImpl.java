package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.Note;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.UserRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.NoteRepository;
import com.telerikacademy.virtualteacher.services.contracts.NoteService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class NoteServiceImpl implements NoteService {
    public static final String USER_TO_NOTE_LECTURE_MUST_BE_ENROLLED_IN_THE_COURSE_THAT_CONTAINS_THAT_LECTURE_ERROR_MESSAGE = "User to note lecture must be enrolled in the course that contains that lecture.";
    private final NoteRepository noteRepository;
     private final UserRepository userRepository;

    public NoteServiceImpl(NoteRepository noteRepository, UserRepository userRepository) {
        this.noteRepository = noteRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Note createNote(Note note, Lecture lecture){
        User userToNote = userRepository.getById(note.getUserId());
        Set<Course> userCourses = userToNote.getUserCourses();

        if(userCourses.stream()
                .anyMatch(course -> course.getCourseLectures()
                        .stream()
                        .anyMatch(lec -> lec.getId() == lecture.getId()))){
            return noteRepository.create(note);
        }
        throw new UnauthorizedOperationException(USER_TO_NOTE_LECTURE_MUST_BE_ENROLLED_IN_THE_COURSE_THAT_CONTAINS_THAT_LECTURE_ERROR_MESSAGE);
    }
}
