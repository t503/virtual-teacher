package com.telerikacademy.virtualteacher.services.contracts;

import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers(User user);

    User getByEmail(String email);

    User getUserById(int id, User user);

    User getUserById(int id);

    User createUser(User newUser);

    boolean deleteUser(User user, int id);

    User updateUser(User userToUpdate, User userUpdating);

    List<User> searchUserBy(Optional<String> email, Optional<String> phone);

    List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email, Optional<String> phoneNumber, User user);

    User enrollCourse(User userToUpdate, Course course);

    List<User> searchUserByMVC(User user,String searchBy,String value);

    List<User> getAllTeachers();

    List<User> getAllUnassignedTeachers(Course course);

    int getAllUsersCount();

}
