package com.telerikacademy.virtualteacher.services.contracts;

import com.telerikacademy.virtualteacher.models.Assignment;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.User;

import java.util.List;

public interface LectureService {
    List<Lecture> getAllLectures();

    Lecture getLectureById(int id);

    Lecture getLectureByTitle(String name);

    Lecture createLecture(Lecture newLecture, User user);

    boolean deleteLecture(int id,User user);

    Lecture updateLecture(Lecture newLecture,User user);

    Lecture addNote(Lecture lecture,User user);

//    Lecture addAssignment(Lecture lecture, Assignment assignment, User user);

}
