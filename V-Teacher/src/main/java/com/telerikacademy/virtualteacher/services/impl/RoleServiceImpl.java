package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.repositories.contracts.RoleRepository;
import com.telerikacademy.virtualteacher.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> getAllRoles(){
        return roleRepository.getAll();
    }

    @Override
    public Role getRoleById(int id){
        return roleRepository.getById(id);
    }

    @Override
    public Role getRoleByName(String name){
        return roleRepository.getByField("name", name);
    }
}
