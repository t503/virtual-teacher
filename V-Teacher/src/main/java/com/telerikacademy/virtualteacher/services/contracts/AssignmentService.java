package com.telerikacademy.virtualteacher.services.contracts;

import com.telerikacademy.virtualteacher.models.Assignment;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.User;

import java.util.List;

public interface AssignmentService {
    List<Assignment> getAll(User user);

    Assignment getById(int id, User user);

    void createAssignment(Assignment assignment, User user, Course course, Lecture lecture);

    List<Lecture> getAllCompletedUserLectures(User user, User userRequesting);

    List<Assignment> getAssignmentsPendingForGrading(User user);

    boolean hasUserSubmitted(User user,Course course ,Lecture lecture);

    Assignment getAssignment(User user, Course course, Lecture lecture);

    Assignment updateAssignment(Assignment assignmentToUpdate, User user);

    List<Assignment> gelAllAssignmentsForUser(User user, User userRequesting);

    List<Assignment> getFilterAssignments(String searchBy, String value);

    List<Assignment> getAssignmentsByCourse(Course course,User user);

    void deleteAssignment(Assignment assignment,User user);
}
