package com.telerikacademy.virtualteacher.services.contracts;

import com.telerikacademy.virtualteacher.models.Topic;

import java.util.List;

public interface TopicService {
    List<Topic> getAllTopics();

    Topic getTopicById(int id);

    Topic getTopicByName(String name);
}
