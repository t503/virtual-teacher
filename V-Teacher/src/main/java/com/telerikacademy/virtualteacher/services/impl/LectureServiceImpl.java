package com.telerikacademy.virtualteacher.services.impl;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Assignment;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.repositories.contracts.CourseRepository;
import com.telerikacademy.virtualteacher.repositories.contracts.LectureRepository;
import com.telerikacademy.virtualteacher.services.contracts.CourseService;
import com.telerikacademy.virtualteacher.services.contracts.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LectureServiceImpl implements LectureService {

    public static final String ONLY_USERS_WITH_ROLE_TEACHER_OR_ADMINISTRATORS_CAN_CREATE_LECTURES_ERROR_MESSAGE = "Only users with role Teacher or Administrators can create lectures";
    public static final String ONLY_USERS_WITH_ROLE_TEACHER_OR_ADMINISTRATORS_CAN_DELETE_LECTURES_ERROR_MESSAGE = "Only users with role Teacher or Administrators can delete lectures";
    public static final String ONLY_USERS_WITH_ROLE_TEACHER_OR_ADMINISTRATORS_CAN_UPDATE_LECTURES_ERROR_MESSAGE = "Only users with role Teacher or Administrators can update lectures";

    private final LectureRepository lectureRepository;
    private final CourseService courseService;

    @Autowired
    public LectureServiceImpl(LectureRepository repository, CourseService courseService) {
        this.lectureRepository = repository;
        this.courseService = courseService;
    }

    @Override
    public List<Lecture> getAllLectures(){
        return lectureRepository.getAll();
    }

    @Override
    public Lecture getLectureById(int id){
        return lectureRepository.getById(id);
    }

    @Override
    public Lecture getLectureByTitle(String title){
        return lectureRepository.getByField("title", title);
    }

    @Override
    public Lecture createLecture(Lecture newLecture, User user){
        if (user.isTeacher() || user.isAdmin()){
            boolean duplicateLectureExists = true;

            try{
                lectureRepository.getByField("title",newLecture.getTitle());
            } catch (EntityNotFoundException e){
                duplicateLectureExists = false;
            }

            if (duplicateLectureExists){
                throw new DuplicateEntityException("Lecture","title",newLecture.getTitle());
            }
            return lectureRepository.create(newLecture);
        }
        throw new UnauthorizedOperationException(ONLY_USERS_WITH_ROLE_TEACHER_OR_ADMINISTRATORS_CAN_CREATE_LECTURES_ERROR_MESSAGE);
    }

    @Override
    public boolean deleteLecture(int id,User user){
        if (user.isTeacher() || user.isAdmin()){
            lectureRepository.delete(id);
            return true;
        }
        throw new UnauthorizedOperationException(ONLY_USERS_WITH_ROLE_TEACHER_OR_ADMINISTRATORS_CAN_DELETE_LECTURES_ERROR_MESSAGE);
    }

    @Override
    public Lecture updateLecture(Lecture lectureToUpdate,User user){
        if (user.isTeacher() || user.isAdmin()){
            boolean duplicateLectureExists = true;

            try{
                Lecture lecture = lectureRepository.getByField("title",lectureToUpdate.getTitle());
                if (lectureToUpdate.getTitle().equalsIgnoreCase(lecture.getTitle())){
                    duplicateLectureExists = false;
                }
            } catch (EntityNotFoundException e){
                duplicateLectureExists = false;
            }

            if (duplicateLectureExists){
                throw new DuplicateEntityException("Lecture","title",lectureToUpdate.getTitle());
            }
            lectureRepository.update(lectureToUpdate);
            return lectureToUpdate;
        }
        throw new UnauthorizedOperationException(ONLY_USERS_WITH_ROLE_TEACHER_OR_ADMINISTRATORS_CAN_UPDATE_LECTURES_ERROR_MESSAGE);
    }

    @Override
    public Lecture addNote(Lecture lecture,User user){
       lectureRepository.update(lecture);
       return lecture;
    }
}