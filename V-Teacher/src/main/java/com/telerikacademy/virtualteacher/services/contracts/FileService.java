package com.telerikacademy.virtualteacher.services.contracts;

import org.springframework.core.io.FileSystemResource;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    String saveFile(MultipartFile file);

    FileSystemResource getFile(String fileName);
}
