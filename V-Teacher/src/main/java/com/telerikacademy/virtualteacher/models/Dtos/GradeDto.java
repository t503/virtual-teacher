package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

public class GradeDto {

    public static final String MINIMUM_ALLOWED_GRADE_ERROR_MESSAGE = "Minimum allowed grade is 2";
    public static final String MAXIMUM_ALLOWED_GRADE_ERROR_MESSAGE = "Maximum allowed grade is 6";

    @Min(value = 2,message = MINIMUM_ALLOWED_GRADE_ERROR_MESSAGE)
    @Max(value = 6,message = MAXIMUM_ALLOWED_GRADE_ERROR_MESSAGE)
    private double grade;

    public GradeDto() {
    }

    public GradeDto(double grade) {
        this.grade = grade;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }
}
