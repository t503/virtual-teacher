package com.telerikacademy.virtualteacher.models;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "lectures")
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lecture_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "video_url")
    private String videoUrl;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "lecture_notes",
            joinColumns = {@JoinColumn(name = "lecture_id")},
            inverseJoinColumns = {@JoinColumn(name = "note_id")})
    private Set<Note> notes;

    @Column(name = "assignment_description")
    private String assignmentDescription;

    public Lecture() {
    }

    public Lecture(int lectureId, String title, String description, String videoUrl,
                   Set<Note> notes,String assignmentDescription) {
        setId(lectureId);
        setTitle(title);
        setTitle(description);
        setVideoUrl(videoUrl);
        setNotes(notes);
        setAssignmentDescription(assignmentDescription);
    }

    public String getAssignmentDescription() {
        return assignmentDescription;
    }

    public void setAssignmentDescription(String assignmentDescription) {
        this.assignmentDescription = assignmentDescription;
    }

    public Set<Note> getNotes() {
        return notes;
    }

    public void setNotes(Set<Note> notes) {
        this.notes = notes;
    }

    public int getId() {
        return id;
    }

    public void setId(int lectureId) {
        this.id = lectureId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lecture)) return false;
        Lecture lecture = (Lecture) o;
        return getId() == lecture.getId() && getTitle().equals(lecture.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle());
    }
}
