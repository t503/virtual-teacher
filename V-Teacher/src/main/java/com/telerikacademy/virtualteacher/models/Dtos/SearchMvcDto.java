package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.NotBlank;

public class SearchMvcDto {

    private String searchBy;

    private String value;

    public SearchMvcDto() {
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
