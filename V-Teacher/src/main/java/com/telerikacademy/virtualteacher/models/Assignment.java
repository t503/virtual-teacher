package com.telerikacademy.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "assignment")
public class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @OneToOne
    @JoinColumn(name = "lecture_id")
    private Lecture lecture;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "isComplete")
    private boolean isComplete;

    @Column(name = "user_grade")
    private double userGrade;


    public Assignment() {
    }

    public Assignment(User user, Lecture lecture, String filePath, boolean isComplete, double userGrade,
                      Course course) {
        setUser(user);
        setLecture(lecture);
        setFilePath(filePath);
        setComplete(isComplete);
        setUserGrade(userGrade);
        setCourse(course);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Lecture getLecture() {
        return lecture;
    }

    public void setLecture(Lecture lecture) {
        this.lecture = lecture;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public double getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(double userGrade) {
        this.userGrade = userGrade;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public boolean hasGrade(){
        return userGrade != 0.0;
    }
}
