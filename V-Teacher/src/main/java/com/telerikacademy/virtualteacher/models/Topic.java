package com.telerikacademy.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "topics")
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "topic_id")
    private int id;

    @Column(name = "name")
    private String name;

    public Topic() {
    }

    public Topic(int topicID, String name) {
        setId(topicID);
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int topicID) {
        this.id = topicID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
