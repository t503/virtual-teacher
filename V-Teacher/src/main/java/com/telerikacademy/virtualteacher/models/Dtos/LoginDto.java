package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class LoginDto {

    public static final String EMAIL_FIELD_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Email field should not be blank.";
    public static final String VALID_EMAIL_EXAMPLE_ERROR_MESSAGE = "Should be valid email address: example@email.com";
    public static final String PASSWORD_FIELD_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Password field should not be blank.";

    @NotEmpty(message = EMAIL_FIELD_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Email(message = VALID_EMAIL_EXAMPLE_ERROR_MESSAGE)
    private String email;

    @NotEmpty(message = PASSWORD_FIELD_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
