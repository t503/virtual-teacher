package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.Positive;

public class AddLectureDto {

    public static final String LECTURE_ID_SHOULD_BE_POSITIVE = "Lecture ID should be positive.";

    @Positive(message = LECTURE_ID_SHOULD_BE_POSITIVE)
    private int lectureId;

    public AddLectureDto() {
    }

    public AddLectureDto(int lectureId) {
        setLectureId(lectureId);
    }

    public int getLectureId() {
        return lectureId;
    }

    public void setLectureId(int lectureId) {
        this.lectureId = lectureId;
    }
}
