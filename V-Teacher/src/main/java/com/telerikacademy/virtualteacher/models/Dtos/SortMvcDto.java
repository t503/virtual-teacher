package com.telerikacademy.virtualteacher.models.Dtos;

public class SortMvcDto {

    private String sort;

    public SortMvcDto() {
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
