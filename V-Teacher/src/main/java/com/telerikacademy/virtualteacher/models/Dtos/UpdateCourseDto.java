package com.telerikacademy.virtualteacher.models.Dtos;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.validation.constraints.*;
import java.time.LocalDate;

public class UpdateCourseDto {

    public static final String COURSE_TOPIC_ID_SHOULD_BE_POSITIVE_NUMBER_ERROR_MESSAGE = "Course topic ID should be positive number.";
    public static final String COURSE_TITTLE_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Course tittle should not be blank.";
    public static final String TITLE_LENGTH_ERROR_MESSAGE = "Title should be between 5 and 50 characters.";
    public static final String COURSE_DESCRIPTION_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Course description should not be blank.";
    public static final String DESCRIPTION_LENGTH_ERROR_MESSAGE = "Description should be between 3 and 1000 characters.";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String MINIMUM_GRADE_ERROR_MESSAGE = "Minimum grade should be equal or above 3.";
    public static final String MAXIMUM_GRADE_ERROR_MESSAGE = "Maximum grade should be equal ot less than 6.";
    @Positive(message = COURSE_TOPIC_ID_SHOULD_BE_POSITIVE_NUMBER_ERROR_MESSAGE)
    private int topicId;

    @NotBlank(message = COURSE_TITTLE_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 5, max = 50, message = TITLE_LENGTH_ERROR_MESSAGE)
    private String title;

    @NotBlank(message = COURSE_DESCRIPTION_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 3, max = 1000, message = DESCRIPTION_LENGTH_ERROR_MESSAGE)
    private String description;

    @DateTimeFormat(pattern = DATE_FORMAT)
    private LocalDate startingDate;

    @Min(value = 3,message = MINIMUM_GRADE_ERROR_MESSAGE)
    @Max(value = 6,message = MAXIMUM_GRADE_ERROR_MESSAGE)
    @Column(name = "min_grade")
    private double mingGrade;

    public UpdateCourseDto() {
    }

    public UpdateCourseDto(String title, int topicId, String description, LocalDate startingDate, double mingGrade) {
        setTitle(title);
        setDescription(description);
        setTopicId(topicId);
        setStartingDate(startingDate);
        setMingGrade(mingGrade);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    public double getMingGrade() {
        return mingGrade;
    }

    public void setMingGrade(double mingGrade) {
        this.mingGrade = mingGrade;
    }
}
