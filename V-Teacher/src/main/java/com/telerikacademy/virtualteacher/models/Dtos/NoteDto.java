package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class NoteDto {

    public static final String NOTE_MESSAGE_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Note message should not be blank.";
    public static final String NOTE_LENGTH_ERROR_MESSAGE = "Note should be between 3 and 1000 characters.";

    @NotBlank(message = NOTE_MESSAGE_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 3, max = 1000,message = NOTE_LENGTH_ERROR_MESSAGE)
    private String note;

    public NoteDto() {
    }

    public NoteDto(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
