package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CourseDto {

    public static final String COURSE_TOPIC_ID_SHOULD_BE_POSITIVE_ERROR_MESSAGE = "Course topic ID should be positive number.";
    public static final String COURSE_TITTLE_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Course tittle should not be blank.";
    public static final String TITLE_SHOULD_BE_BETWEEN_5_AND_50_CHARACTERS_ERROR_MESSAGE = "Title should be between 5 and 50 characters.";
    public static final String COURSE_DESCRIPTION_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Course description should not be blank.";
    public static final String DESCRIPTION_SHOULD_BE_BETWEEN_3_AND_1000_CHARACTERS_ERROR_MESSAGE = "Description should be between 3 and 1000 characters.";

    @Positive(message = COURSE_TOPIC_ID_SHOULD_BE_POSITIVE_ERROR_MESSAGE)
    private int topicId;

    @NotBlank(message = COURSE_TITTLE_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 5, max = 50, message = TITLE_SHOULD_BE_BETWEEN_5_AND_50_CHARACTERS_ERROR_MESSAGE)
    private String title;

    @NotBlank(message = COURSE_DESCRIPTION_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 3, max = 1000, message = DESCRIPTION_SHOULD_BE_BETWEEN_3_AND_1000_CHARACTERS_ERROR_MESSAGE)
    private String description;

    public CourseDto() {
    }

    public CourseDto(String title, int topicId, String description) {
        this.title = title;
        this.topicId = topicId;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
