package com.telerikacademy.virtualteacher.models;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "user_courses")
public class EnrolledCourses {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "rating")
    private Double rating;

    @Column(name = "final_grade")
    private double finalGrade;

    @JsonIgnore
    private boolean isComplete;

    public EnrolledCourses() {
    }

    public EnrolledCourses(User user, Course course, Double rating, double finalGrade, boolean isComplete) {
        setUser(user);
        setCourse(course);
        setRating(rating);
        setFinalGrade(finalGrade);
    }
    public EnrolledCourses(User user, Course course, double rating) {
        setUser(user);
        setCourse(course);
        setRating(rating);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public double getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(double finalGrade) {
        this.finalGrade = finalGrade;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    @JsonIgnore
    public boolean isGrated(){
        if (this.finalGrade != 0){
            return true;
        }
        return false;
    }
}

