package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class RatingDto {

    public static final String MIN_RATING_SCORE_ERROR_MESSAGE = "Rating score should be above 1 and less than 10.";
    public static final String MAX_RATING_ERROR_MESSAGE = "Rating score should be above 1 and less than 10.";

    @Min(value = 1,message = MIN_RATING_SCORE_ERROR_MESSAGE)
    @Max(value = 10,message = MAX_RATING_ERROR_MESSAGE)
    private double rating;

    public RatingDto() {
    }

    public RatingDto(double rating) {
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}

