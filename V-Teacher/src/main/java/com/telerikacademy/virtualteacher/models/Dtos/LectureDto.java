package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class LectureDto {

    public static final String LECTURE_TITLE_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Lecture title should not be blank.";
    public static final String TITLE_LENGTH_ERROR_MESSAGE = "Title should be between 5 and 50 characters";
    public static final String LECTURE_DESCRIPTION_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Lecture description should not be blank.";
    public static final String DESCRIPTION_LENGTH_ERROR_MESSAGE = "Description should be between 3 and 1000 characters";
    public static final String VIDEO_URL_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Video URL should not be blank.";
    public static final String URL_LENGTH_ERROR_MESSAGE = "URL should be between 3 and 5000 characters";
    public static final String ASSIGNMENT_DESCRIPTION_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE = "Lecture description should not be blank.";

    @NotBlank(message = LECTURE_TITLE_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 5, max = 50, message = TITLE_LENGTH_ERROR_MESSAGE)
    private String title;

    @NotBlank(message = LECTURE_DESCRIPTION_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 3, max = 1000, message = DESCRIPTION_LENGTH_ERROR_MESSAGE)
    private String description;

    @NotBlank(message = VIDEO_URL_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 3, max = 500, message = URL_LENGTH_ERROR_MESSAGE)
    private String videoUrl;

    @NotBlank(message = LectureDto.ASSIGNMENT_DESCRIPTION_SHOULD_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 3, max = 1000, message = LectureDto.DESCRIPTION_LENGTH_ERROR_MESSAGE)
    private String assignmentDescription;

    public LectureDto() {
    }

    public LectureDto(String title, String description, String videoUrl, String assignmentDescription) {
        setTitle(title);
        setDescription(description);
        setVideoUrl(videoUrl);
        setAssignmentDescription(assignmentDescription);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getAssignmentDescription() {
        return assignmentDescription;
    }

    public void setAssignmentDescription(String assignmentDescription) {
        this.assignmentDescription = assignmentDescription;
    }
}
