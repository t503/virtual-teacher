package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.Positive;

public class EnrollCourseDto {

    public static final String COURSE_ID_SHOULD_BE_POSITIVE_ERROR_MESSAGE = "Course id should be positive.";

    @Positive(message = COURSE_ID_SHOULD_BE_POSITIVE_ERROR_MESSAGE)
    private int courseId;

    public EnrollCourseDto() {
    }

    public EnrollCourseDto(int courseId) {
        this.courseId = courseId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

}
