package com.telerikacademy.virtualteacher.models.Dtos;

import org.springframework.web.multipart.MultipartFile;

public class AssignmentDto {

    public static final String ACCEPTABLE_FILE_TYPES_ERROR_MESSAGE = "Acceptable file types: txt and pdf.";

    private MultipartFile file;

    public AssignmentDto() {
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        if (!file.getContentType().equalsIgnoreCase("text/plain") &&
                !file.getContentType().equalsIgnoreCase("application/pdf")) {
            throw new IllegalArgumentException(ACCEPTABLE_FILE_TYPES_ERROR_MESSAGE);
        }

        this.file = file;
    }


}
