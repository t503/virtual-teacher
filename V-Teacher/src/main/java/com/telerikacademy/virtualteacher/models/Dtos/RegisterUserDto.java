package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.*;

public class RegisterUserDto {

    public static final String FIRST_NAME_CANNOT_BE_BLANK_ERROR_MESSAGE = "First name cannot be blank";
    public static final String FIRST_NAME_LENGTH_ERROR_MESSAGE = "First name should be between 2 and 20 characters";
    public static final String LAST_NAME_CANNOT_BE_BLANK_ERROR_MESSAGE = "Last name cannot be blank";
    public static final String LAST_NAME_LENGTH_ERROR_MESSAGE = "Last name should be between 2 and 20 characters";
    public static final String EMAIL_CANNOT_BE_BLANK_ERROR_MESSAGE = "Email cannot be blank";
    public static final String VALID_EMAIL_EXAMPLE_ERROR_MESSAGE = "Should be valid email: example@email.com";
    public static final String EMAIL_LENGTH_ERROR_MESSAGE = "Email should be between 3 and 40 characters";
    public static final String PASSWORD_MUST_NOT_BE_BLANK_ERROR_MESSAGE = "Password must not be blank";
    public static final String PASSWORD_LENGTH_ERROR_MESSAGE = "Password should at least 8 symbols";
    public static final String PASSWORD_FORMAT_ERROR_MESSAGE = "Password should have at least one Uppercase letter, one special symbol(@#$%^&+=) and at least one digit";
    public static final String CONFIRM_PASSWORD_ERROR_MESSAGE = "Confirm password should match the password and not be blank.";
    public static final String CONFIR_PASSWORD_ERROR_MESSAGE = "Password should be between 3 and 30 characters";
    public static final String REGISTER_AS_STUDENT = "Register as student";
    public static final String REGISTER_AS_TEACHER = "Register as teacher";

    @NotBlank(message = FIRST_NAME_CANNOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 2, max = 20, message = FIRST_NAME_LENGTH_ERROR_MESSAGE)
    private String firstName;

    @NotBlank(message = LAST_NAME_CANNOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 2, max = 20, message = LAST_NAME_LENGTH_ERROR_MESSAGE)
    private String lastName;

    @NotBlank(message = EMAIL_CANNOT_BE_BLANK_ERROR_MESSAGE)
    @Email(message = VALID_EMAIL_EXAMPLE_ERROR_MESSAGE)
    @Size(min = 3, max = 40, message = EMAIL_LENGTH_ERROR_MESSAGE)
    private String email;

    @NotBlank(message = PASSWORD_MUST_NOT_BE_BLANK_ERROR_MESSAGE)
    @Size(min = 8, max = 30, message = PASSWORD_LENGTH_ERROR_MESSAGE)
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$",
            message = PASSWORD_FORMAT_ERROR_MESSAGE)
    private String password;

    @NotBlank(message = CONFIRM_PASSWORD_ERROR_MESSAGE)
    @Size(min = 8, max = 30, message = CONFIR_PASSWORD_ERROR_MESSAGE)
    private String confirmPassword;

    @Min(value = 1,message = REGISTER_AS_STUDENT)
    @Max(value = 2,message = REGISTER_AS_TEACHER)
    private int roleId;

    public RegisterUserDto() {
    }

    public RegisterUserDto(String password, String confirmPassword, String firstName, String lastName, String email, int roleId) {
        setPassword(password);
        setConfirmPassword(confirmPassword);
        setFirstName(firstName);
        setLastName(lastName);
        setEmail(email);
        setRoleId(roleId);
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
