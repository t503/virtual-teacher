package com.telerikacademy.virtualteacher.models.Dtos;

import org.springframework.web.multipart.MultipartFile;

public class PhotoMvcDto {

    private MultipartFile file;

    public PhotoMvcDto() {
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        if (!file.getContentType().equalsIgnoreCase("image/jpeg") &&
                !file.getContentType().equalsIgnoreCase("image/png") &&
                !file.getContentType().equalsIgnoreCase("image/svg+xml") &&
                !file.getContentType().equalsIgnoreCase("image/tiff")){
            throw new IllegalArgumentException("Acceptable file types: jpg,png,svg and tiff.");
        }
        this.file = file;
    }
}
