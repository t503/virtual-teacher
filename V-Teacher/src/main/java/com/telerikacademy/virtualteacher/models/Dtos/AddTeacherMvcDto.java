package com.telerikacademy.virtualteacher.models.Dtos;

import javax.validation.constraints.Positive;

public class AddTeacherMvcDto {

    public static final String TEACHER_ID_SHOULD_BE_POSITIVE = "Teacher ID should be positive.";

    @Positive(message = TEACHER_ID_SHOULD_BE_POSITIVE)
    private int teacherId;

    public AddTeacherMvcDto() {
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }
}
