package com.telerikacademy.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private int id;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;

    @Column(name = "description")
    private String description;

    @Column(name = "starting_date")
    private LocalDate startingDate;

    @Column(name = "picture_url")
    private String picture;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "course_lectures",
            joinColumns = {@JoinColumn(name = "course_id")},
            inverseJoinColumns = {@JoinColumn(name = "lecture_id")})
    private Set<Lecture> courseLectures;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "course_teachers",
            joinColumns = {@JoinColumn(name = "course_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<User> courseTeachers;

    @Column(name = "rating")
    private double rating;

    @Column(name = "min_grade")
    private double mingGrade;

    public Course() {
    }

    public Course(int courseId, String title, Topic topic, String description, LocalDate startingDate,
                  Set<Lecture> courseLectures, double rating, Set<User> courseTeachers,
                  double minGrade) {
        setId(courseId);
        setTitle(title);
        setTopic(topic);
        setDescription(description);
        setStartingDate(startingDate);
        setCourseLectures(courseLectures);
        setRating(rating);
        setCourseTeachers(courseTeachers);
        setMingGrade(minGrade);
    }

    public double getMingGrade() {
        return mingGrade;
    }

    public void setMingGrade(double mingGrade) {
        this.mingGrade = mingGrade;
    }

    public Set<User> getCourseTeachers() {
        return courseTeachers;
    }

    public void setCourseTeachers(Set<User> courseTeachers) {
        this.courseTeachers = courseTeachers;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Set<Lecture> getCourseLectures() {
        return courseLectures;
    }

    public void setCourseLectures(Set<Lecture> courseLectures) {
        this.courseLectures = courseLectures;
    }

    public int getId() {
        return id;
    }

    public void setId(int courseId) {
        this.id = courseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Course)) return false;
        Course course = (Course) o;
        return getId() == course.getId() && getTitle().equals(course.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle());
    }

    @JsonIgnore
    public boolean isPublished() {
        if (this.getStartingDate() != null) {
            return true;
        }
        return false;
    }

    @JsonIgnore
    public boolean hasDateOccurred() {
        if (LocalDate.now().compareTo(this.getStartingDate()) > 0) {
            return true;
        }
        return false;
    }
}
