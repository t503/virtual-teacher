package com.telerikacademy.virtualteacher.utils;

import com.telerikacademy.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";
    public static final String THE_REQUESTED_RESOURCE_REQUIRES_AUTHENTICATION_ERROR_MESSAGE = "The requested resource requires authentication";
    public static final String INVALID_USER = "Invalid user";
    public static final String USERS_CAN_ONLY_MODIFY_THEIR_OWN_PROFILES_ERROR_MESSAGE = "Users can only modify their own profiles";
    public static final String NO_USER_LOGGED_IN_INVALID_USER = "No user logged in.";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    THE_REQUESTED_RESOURCE_REQUIRES_AUTHENTICATION_ERROR_MESSAGE);
        }
        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_USER);
        }
    }


    public boolean isTheSameUser(HttpHeaders headers, User user) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    AuthenticationHelper.THE_REQUESTED_RESOURCE_REQUIRES_AUTHENTICATION_ERROR_MESSAGE);
        }
        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            User requestUser = userService.getByEmail(email);
            if (requestUser.getId() != user.getId()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                        USERS_CAN_ONLY_MODIFY_THEIR_OWN_PROFILES_ERROR_MESSAGE);
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_USER);
        }
        return true;
    }

    public User verifyAuthentication(String email, String password) {
        try {
            User user = userService.getByEmail(email);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }
    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthenticationFailureException(NO_USER_LOGGED_IN_INVALID_USER);
        }
        return userService.getByEmail(currentUser);
    }
}
