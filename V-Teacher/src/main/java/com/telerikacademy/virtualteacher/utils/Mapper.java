package com.telerikacademy.virtualteacher.utils;

import com.telerikacademy.virtualteacher.models.*;
import com.telerikacademy.virtualteacher.models.Dtos.*;
import com.telerikacademy.virtualteacher.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class Mapper {

    private final UserService userService;
    private final CourseService courseService;
    private final LectureService lectureService;
    private final TopicService topicService;
    private final RoleService roleService;
    private final FileService fileService;
    private final RoleRequestService roleRequestService;


    @Autowired
    public Mapper(UserService userService, LectureService lectureService,
                  CourseService courseService, TopicService topicService,
                  RoleService roleService, FileService fileService, RoleRequestService roleRequestService) {
        this.userService = userService;
        this.lectureService = lectureService;
        this.courseService = courseService;
        this.topicService = topicService;
        this.roleService = roleService;
        this.fileService = fileService;
        this.roleRequestService = roleRequestService;
    }

    public RegisterUserDto toUserDto(User user) {
        RegisterUserDto registerUserDto = new RegisterUserDto();
        registerUserDto.setPassword(user.getPassword());
        registerUserDto.setConfirmPassword(user.getPassword());
        registerUserDto.setFirstName(user.getFirstName());
        registerUserDto.setLastName(user.getLastName());
        registerUserDto.setEmail(user.getEmail());
        return registerUserDto;
    }

    public User fromDto(RegisterUserDto dto) {
        User studentToCreate = new User();
        studentToCreate.setPassword(dto.getPassword());
        studentToCreate.setFirstName(dto.getFirstName());
        studentToCreate.setLastName(dto.getLastName());
        studentToCreate.setEmail(dto.getEmail());
        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getRoleById(1));
        studentToCreate.setUserRole(roles);

        return studentToCreate;
    }

    public User fromDto(UpdateUserDto dto, int id) {
        User userToUpdate = userService.getUserById(id);
        userToUpdate.setPassword(dto.getPassword());
        userToUpdate.setFirstName(dto.getFirstName());
        userToUpdate.setLastName(dto.getLastName());
        userToUpdate.setEmail(dto.getEmail());
        userToUpdate.setAboutUser(dto.getAboutUser());


        return userToUpdate;
    }

    public User fromDto(EnrollCourseDto dto, int id) {
        User userToEnrollCourse = userService.getUserById(id);
        Course course = courseService.getCourseById(dto.getCourseId());
        Set<Course> courses = userToEnrollCourse.getUserCourses();
        courses.add(course);
        userToEnrollCourse.setUserCourses(courses);

        return userToEnrollCourse;
    }

    public UpdateUserDto toUserDtoo(User user) {
        UpdateUserDto userDto = new UpdateUserDto();
        userDto.setPassword(user.getPassword());
        userDto.setConfirmPassword(user.getPassword());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setAboutUser(user.getAboutUser());
        return userDto;
    }

    public Lecture fromDto(LectureDto dto) {

        Lecture lectureToCreate = new Lecture();
        lectureToCreate.setTitle(dto.getTitle());
        lectureToCreate.setDescription(dto.getDescription());
        lectureToCreate.setVideoUrl(dto.getVideoUrl());
        lectureToCreate.setAssignmentDescription(dto.getAssignmentDescription());

        return lectureToCreate;
    }

    public Lecture fromDto(LectureDto dto, int id) {
        Lecture lectureToUpdate = lectureService.getLectureById(id);

        lectureToUpdate.setTitle(dto.getTitle());
        lectureToUpdate.setDescription(dto.getDescription());
        lectureToUpdate.setVideoUrl(dto.getVideoUrl());
        lectureToUpdate.setAssignmentDescription(dto.getAssignmentDescription());

        return lectureToUpdate;
    }

    public Course addTeacher(int courseId,int teacherId){
        Course course = courseService.getCourseById(courseId);
        User user = userService.getUserById(teacherId);
        Set<User> courseTeachers = course.getCourseTeachers();
        courseTeachers.add(user);

        return course;
    }

    public Course fromDto(CourseDto dto) {
        Course courseToCreate = new Course();
        courseToCreate.setTitle(dto.getTitle());
        Topic topic = topicService.getTopicById(dto.getTopicId());
        courseToCreate.setTopic(topic);
        courseToCreate.setDescription(dto.getDescription());
        courseToCreate.setMingGrade(3);
        return courseToCreate;
    }

    public Course fromDto(CourseDto dto, int id) {
        Course courseToUpdate = courseService.getCourseById(id);

        courseToUpdate.setTitle(dto.getTitle());

        Topic topic = topicService.getTopicById(dto.getTopicId());
        courseToUpdate.setTopic(topic);

        courseToUpdate.setDescription(dto.getDescription());

        return courseToUpdate;
    }

    public UpdateCourseDto toUpdateCourseDto(Course course){
        UpdateCourseDto dto = new UpdateCourseDto();

        dto.setTitle(course.getTitle());
        dto.setDescription(course.getDescription());
        dto.setTopicId(course.getTopic().getId());
        dto.setStartingDate(course.getStartingDate());
        dto.setMingGrade(course.getMingGrade());

        return dto;
    }

    public Course fromDto(UpdateCourseDto dto, int id){
        Course course = courseService.getCourseById(id);

        course.setTitle(dto.getTitle());
        course.setDescription(dto.getDescription());
        Topic topic = topicService.getTopicById(dto.getTopicId());
        course.setTopic(topic);
        course.setStartingDate(dto.getStartingDate());
        course.setMingGrade(dto.getMingGrade());

        return course;
    }


    public Note fromDto(NoteDto dto, User user) {
        Note note = new Note();
        note.setNote(dto.getNote());
        note.setUserId(user.getId());

        return note;
    }

    public Lecture setNote(int lectureId, Note note) {
        Lecture lecture = lectureService.getLectureById(lectureId);
        Set<Note> lectureNotes = lecture.getNotes();
        lectureNotes.add(note);
        lecture.setNotes(lectureNotes);
        return lecture;
    }

    public Assignment fromDto(AssignmentDto assignmentDto,User user,Course course, Lecture lecture){
        Assignment assignment = new Assignment();
        assignment.setUser(user);
        assignment.setCourse(course);
        assignment.setLecture(lecture);
        assignment.setFilePath(fileService.saveFile(assignmentDto.getFile()));
        assignment.setComplete(true);

        return assignment;
    }


}
