package com.telerikacademy.virtualteacher.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public static final String ENTITY_NOT_FOUND_THREE_ATTRIBUTES_ERROR_MESSAGE = "%s with %s %s not found.";
    public static final String NO_SUCH_ENTITY_FOUND_ERROR_MESSAGE = "No such %s were found";

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format(ENTITY_NOT_FOUND_THREE_ATTRIBUTES_ERROR_MESSAGE, type, attribute, value));
    }

    public EntityNotFoundException(String type) {
        super(String.format(NO_SUCH_ENTITY_FOUND_ERROR_MESSAGE, type));
    }

}
