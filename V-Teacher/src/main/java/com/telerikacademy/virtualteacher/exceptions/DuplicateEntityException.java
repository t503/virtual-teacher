package com.telerikacademy.virtualteacher.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public static final String SUCH_ENTITY_ALREADY_EXISTS_ERROR_MESSAGE = "%s with %s %s already exists.";

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format(SUCH_ENTITY_ALREADY_EXISTS_ERROR_MESSAGE, type, attribute, value));
    }

    public DuplicateEntityException(String message) {
        super(message);
    }

}
