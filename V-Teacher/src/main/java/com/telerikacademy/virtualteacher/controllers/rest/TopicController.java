package com.telerikacademy.virtualteacher.controllers.rest;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Topic;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.services.contracts.TopicService;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/topics")
public class TopicController {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private final TopicService topicService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final Mapper mapper;

    @Autowired
    public TopicController(TopicService topicService, UserService userService, AuthenticationHelper authenticationHelper, Mapper mapper) {
        this.topicService = topicService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
    }

    @GetMapping()
    public List<Topic> getAllTopics(@RequestHeader(required = false) HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
        return topicService.getAllTopics();
    }

    @GetMapping("/{id}")
    public Topic getTopicById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return topicService.getTopicById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/name")
    public Topic getTopicByName(@RequestHeader HttpHeaders headers, @RequestParam(required = false) String name) {
        try {
            authenticationHelper.tryGetUser(headers);
            User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return topicService.getTopicByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }



}
