package com.telerikacademy.virtualteacher.controllers.rest;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Dtos.*;
import com.telerikacademy.virtualteacher.models.User;
//import com.telerikacademy.virtualteacher.models.UserEnrollCourse;
import com.telerikacademy.virtualteacher.services.contracts.CourseService;
import com.telerikacademy.virtualteacher.services.contracts.FileService;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
//import com.telerikacademy.virtualteacher.services.impl.UserEnrollCourseService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String FILTER_VALUES = "Filters by one or more parameters: firstName , lastName , email , phoneNumber.";

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final Mapper mapper;
    private final CourseService courseService;
    private final FileService fileService;

    @Autowired
    public UserController(UserService userService, AuthenticationHelper authenticationHelper,
                          Mapper mapper, CourseService courseService, FileService fileService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
        this.courseService = courseService;
        this.fileService = fileService;
    }


    @GetMapping()
    public List<User> getAllUsers(@RequestHeader(required = false) HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getAllUsers(user);
    }

    @GetMapping("/{id}")
    public User getUserById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getUserById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/get/email")
    public User getUserByEmail(@RequestHeader HttpHeaders headers, @RequestParam(required = false) String email) {
        try {
            authenticationHelper.tryGetUser(headers);
            User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/register")
    public User registerUser(@Valid @RequestBody RegisterUserDto dto) {
        try {
            User userToCreate = mapper.fromDto(dto);
            return userService.createUser(userToCreate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User updateUser(@RequestHeader HttpHeaders headers,
                           @PathVariable int id, @Valid @RequestBody UpdateUserDto dto) {
        try {
            User userUpdating = authenticationHelper.tryGetUser(headers);
            User userToUpdate = mapper.fromDto(dto, id);
            userService.updateUser(userToUpdate, userUpdating);
            return userToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("{id}")
    public boolean deleteUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            User userToDelete = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return userService.deleteUser(userToDelete, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> searchByField(@RequestHeader HttpHeaders headers,
                                    @RequestParam(required = false) String email,
                                    @RequestParam(required = false) String phone) {
        try {
            authenticationHelper.tryGetUser(headers);
            return userService.searchUserBy(Optional.ofNullable(email), Optional.ofNullable(phone));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    @ApiOperation(value = "filterBy", notes = FILTER_VALUES)
    public List<User> filter(@RequestHeader HttpHeaders headers, Optional<String> firstName, Optional<String> lastName, Optional<String> email, Optional<String> phoneNumber) {
        User user = authenticationHelper.tryGetUser(headers);
        try {
            return userService.filter(firstName, lastName, email, phoneNumber, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/enroll/course")
    public User userEnrollCourse(@RequestHeader HttpHeaders headers,
                                 @PathVariable int id,
                                 @Valid @RequestBody EnrollCourseDto dto) {
        try {
            authenticationHelper.isTheSameUser(headers, authenticationHelper.tryGetUser(headers));
            User userToEnroll = mapper.fromDto(dto, id);
            Course course = courseService.getCourseById(dto.getCourseId());
            userService.enrollCourse(userToEnroll, course);
            return userToEnroll;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/upload/photo")
    public void uploadUserPhoto(@RequestHeader HttpHeaders headers,@RequestParam("file") MultipartFile file){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            user.setProfilePicture(fileService.saveFile(file));
            userService.updateUser(user,user);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }

    }


}
