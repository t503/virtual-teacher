package com.telerikacademy.virtualteacher.controllers.rest;

import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.services.contracts.RoleService;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/roles")
public class RoleController {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private final RoleService roleService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final Mapper mapper;

    @Autowired
    public RoleController(RoleService roleService, UserService userService, AuthenticationHelper authenticationHelper, Mapper mapper) {
        this.roleService = roleService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
    }

    @GetMapping()
    public List<Role> getAllRoles(@RequestHeader(required = false) HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
        return roleService.getAllRoles();
    }

    @GetMapping("/{id}")
    public Role getRoleById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return roleService.getRoleById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/name")
    public Role getRoleByName(@RequestHeader HttpHeaders headers, @RequestParam(required = false) String name) {
        try {
            authenticationHelper.tryGetUser(headers);
            User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return roleService.getRoleByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
