package com.telerikacademy.virtualteacher.controllers.mvc;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;

@Controller
@RequestMapping("/storage")
public class FileUploadController {

    public static final String FILE_UPLOADED_SUCCESSFULLY = "File uploaded successfully";

    @GetMapping
    public String uploadPage(){
        return "uploadPage";
    }

    @PostMapping("/upload")
    public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file) {
        String fileName = file.getOriginalFilename();

        try{
            file.transferTo(new File("C:\\Users\\petar\\IdeaProjects\\virtual-teacher\\V-Teacher\\src\\main\\resources\\files\\" + fileName));
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(FILE_UPLOADED_SUCCESSFULLY);
    }

}
