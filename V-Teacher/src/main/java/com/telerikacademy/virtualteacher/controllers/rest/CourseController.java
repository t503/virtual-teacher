package com.telerikacademy.virtualteacher.controllers.rest;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Dtos.CourseDto;
import com.telerikacademy.virtualteacher.models.Dtos.RatingDto;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.services.contracts.CourseService;
import com.telerikacademy.virtualteacher.services.contracts.EnrolledCoursesService;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/courses")
public class CourseController {
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String FILTER_VALUES = "Filters by one or more parameters: title , topicId , teacherId , rating.";
    public static final String SORT_VALUES = "Sort by one or more parameters: title , rating.";

    private final CourseService courseService;
    private final Mapper mapper;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final EnrolledCoursesService enrolledCoursesService;

    public CourseController(CourseService courseService, Mapper mapper, UserService userService, AuthenticationHelper authenticationHelper, EnrolledCoursesService enrolledCoursesService) {
        this.courseService = courseService;
        this.mapper = mapper;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.enrolledCoursesService = enrolledCoursesService;
    }

    @GetMapping("/all")
    public List<Course> getAllCourses(@RequestHeader(required = false) HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
        return courseService.getAllCourses();
    }

    @GetMapping()
    public List<Course> getAllCourses() {
        return courseService.getAllCourses();
    }

    @GetMapping("/{id}")
    public Course getCourseById(@PathVariable int id) {
        try {
            return courseService.getCourseById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/display")
    public Course getCourseById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return courseService.getCourseById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/title")
    public Course getCourseByTitle(@RequestParam(required = false) String title) {
        try {
            return courseService.getCourseByTitle(title);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/title/display")
    public Course getCourseByTitle(@RequestHeader HttpHeaders headers, @RequestParam(required = false) String title) {
        try {
            authenticationHelper.tryGetUser(headers);
            userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return courseService.getCourseByTitle(title);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("{id}")
    public boolean deleteCourse(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return courseService.deleteCourse(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/create")
    public Course createCourse(@RequestHeader HttpHeaders headers, @Valid @RequestBody CourseDto dto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course courseToCreate = mapper.fromDto(dto);
            courseService.createCourse(courseToCreate, user);
            return courseToCreate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Course updateCourse(@RequestHeader HttpHeaders headers,
                               @PathVariable int id, @Valid @RequestBody CourseDto dto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course courseToUpdate = mapper.fromDto(dto, id);
            courseService.updateCourse(courseToUpdate, user);
            return courseToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/teacher/{teacherId}")
    public Course addTeacherToCourse(@RequestHeader HttpHeaders headers,
                                     @PathVariable int id, @PathVariable int teacherId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return courseService.addTeacher(teacherId, id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    @ApiOperation(value = "filterBy", notes = FILTER_VALUES)
    public List<Course> filter(Optional<String> title,
                               Optional<Integer> topicId,
                               Optional<Integer> teacherId,
                               Optional<Double> rating) {
        try {
            return courseService.filter(title, topicId, teacherId, rating);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/sort")
    @ApiOperation(value = "sortBy", notes = SORT_VALUES)
    public List<Course> sort(Optional<String> title, Optional<String> rating) {
        try {
            return courseService.sort(title, rating);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PostMapping("/{id}/rating")
    public void rate(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestBody RatingDto dto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            var course = courseService.getCourseById(id);
            enrolledCoursesService.rate(course, user, dto.getRating());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }

    }
}
