package com.telerikacademy.virtualteacher.controllers.mvc;

import com.telerikacademy.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.*;
import com.telerikacademy.virtualteacher.models.Dtos.PhotoMvcDto;
import com.telerikacademy.virtualteacher.models.Dtos.SearchMvcDto;
import com.telerikacademy.virtualteacher.models.Dtos.SortMvcDto;
import com.telerikacademy.virtualteacher.services.contracts.*;
import com.telerikacademy.virtualteacher.services.contracts.CourseService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.models.Dtos.UpdateUserDto;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import com.telerikacademy.virtualteacher.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final CourseService courseService;
    private final AuthenticationHelper authenticationHelper;
    private final Mapper mapper;
    private final FileService fileService;
    private final RoleRequestService roleRequestService;
    private final RoleService roleService;
    private final EnrolledCoursesService enrolledCoursesService;
    private final LectureService lectureService;
    private final AssignmentService assignmentService;

    @Autowired
    public UserMvcController(UserService userService, CourseService courseService,
                             AuthenticationHelper authenticationHelper, Mapper mapper,
                             FileService fileService, RoleRequestService roleRequestService,
                             RoleService roleService, EnrolledCoursesService enrolledCoursesService,
                             LectureService lectureService, AssignmentService assignmentService) {
        this.userService = userService;
        this.courseService = courseService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
        this.fileService = fileService;
        this.roleRequestService = roleRequestService;
        this.roleService = roleService;
        this.enrolledCoursesService = enrolledCoursesService;
        this.lectureService = lectureService;
        this.assignmentService = assignmentService;
    }

    @GetMapping("/page")
    public String showUserPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("activeCourses", courseService.getAllPublishedCourses().size());
            model.addAttribute("allLectures", lectureService.getAllLectures().size());
            model.addAttribute("allStudents", userService.getAllUsersCount());
            return "user-home-page";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/{id}")
    public String showUserProfilePage(@PathVariable int id, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
            User user = userService.getUserById(id);
            List<EnrolledCourses> completeCourses = enrolledCoursesService.getUserCompletedEnrolledCourses(user);
            model.addAttribute("userDto", new UpdateUserDto());
            model.addAttribute("userToView", user);
            model.addAttribute("completeCourses", completeCourses);
            model.addAttribute("userAssignments", assignmentService.gelAllAssignmentsForUser(user, user));
            return "view-user-profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }

    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            userService.deleteUser(user, id);
            return "redirect:/users/all";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("all")
    public String showAllStudent(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("allStudents", userService.getAllUsers(user));
            model.addAttribute("userDtoSearch", new SearchMvcDto());
            return "all-students";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/all")
    public String searchForUser(@Valid @ModelAttribute("userDtoSearch") SearchMvcDto userDto, HttpSession session,
                                Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<User> users = userService.searchUserByMVC(user, userDto.getSearchBy(), userDto.getValue());
            model.addAttribute("allStudents", users);
            return "all-students";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (IndexOutOfBoundsException e) {
            model.addAttribute("error", e.getMessage());
            return "something-went-wrong";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            UpdateUserDto userDto = mapper.toUserDtoo(user);
            List<EnrolledCourses> completeCourses = enrolledCoursesService.getUserCompletedEnrolledCourses(user);

            model.addAttribute("userAssignments", assignmentService.gelAllAssignmentsForUser(user, user));
            model.addAttribute("completeCourses", completeCourses);
            model.addAttribute("userId", id);
            model.addAttribute("userDto", userDto);
            model.addAttribute("user", user);

            return "profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("userDto") UpdateUserDto dto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
        if (errors.hasErrors()) {
            return "profile";
        }
        try {
            User user = mapper.fromDto(dto, id);
            userService.updateUser(user, user);
            session.setAttribute("currentUser", user.getEmail());
            return "profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/upload/photo")
    public String showUploadProfilePhoto(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("newPhoto", new PhotoMvcDto());
            return "change-user-photo";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/upload/photo")
    public String uploadProfilePhoto(HttpSession session,
                                     @Valid @ModelAttribute("newPhoto") PhotoMvcDto userPhotoDto,
                                     Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            MultipartFile file = userPhotoDto.getFile();
            String photo = fileService.saveFile(file);
            user.setProfilePicture(photo);
            userService.updateUser(user, user);
            return "redirect:/users/page";

        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/courses/enrolled")
    public String showUserEnrolledCourses(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("enrolledCoursesUncompleted", enrolledCoursesService.getUserUncompletedEnrolledCourses(user));
            return "user-courses-enrolled";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/courses/completed")
    public String showUserCompletedCourses(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("enrolledCoursesCompleted", enrolledCoursesService.getUserCompletedEnrolledCourses(user));
            return "user-courses-completed";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/courses/all")
    public String showAllCourses(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("courses", courseService.getAllCourses());
            return "user-all-courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/courses/sort")
    public String showAllCoursesSort(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("courses", courseService.getAllCourses());
            model.addAttribute("sortedCourses", new SortMvcDto());
            return "user-all-courses-sort";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @PostMapping("/courses/sort")
    public String sortCourse(@ModelAttribute("sortedCourses") SortMvcDto sortDto,
                             HttpSession session,
                             Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Course> sortedCourses = courseService.sortedMvcCourses(sortDto.getSort());
            model.addAttribute("courses", sortedCourses);
            return "user-all-courses-sort";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (IndexOutOfBoundsException e) {
            model.addAttribute("error", e.getMessage());
            return "something-went-wrong";
        }
    }

    @GetMapping("/courses/search")
    public String showAllCoursesFiltered(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("courses", courseService.getAllCourses());
            model.addAttribute("courseSearchDto", new SearchMvcDto());
            return "user-all-courses-filter";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @PostMapping("/courses/search")
    public String searchForCourse(@ModelAttribute("courseSearchDto") SearchMvcDto searchDto,
                                  HttpSession session,
                                  Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Course> courses = courseService.searchCourseByMVC(searchDto.getSearchBy(), searchDto.getValue());
            model.addAttribute("courses", courses);
            return "user-all-courses-filter";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (IndexOutOfBoundsException e) {
            model.addAttribute("error", e.getMessage());
            return "something-went-wrong";
        }
    }

    @PostMapping("/enroll/course/{id}")
    public String enrollCourse(@PathVariable int id, HttpSession session, Model model) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = courseService.getCourseById(id);
            Set<Course> userCourses = user.getUserCourses();
            userCourses.add(course);
            user.setUserCourses(userCourses);
            userService.enrollCourse(user, course);

            return "redirect:/users/courses/enrolled";

        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/approve")
    public String showUsersToApprove(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("userToBeApprove", roleRequestService.getAllRoleRequests(user));
            return "approve-users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/request/admin")
    public String getAdminRight(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Role role = roleService.getRoleById(3);
            RoleRequest roleRequest = new RoleRequest(user, role);
            roleRequestService.createRoleRequest(roleRequest, user, role);
            return "redirect:/users/page";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "something went wrong";
        }
    }

    @GetMapping("/request/teacher")
    public String getTeacherRight(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Role role = roleService.getRoleById(2);
            RoleRequest roleRequest = new RoleRequest(user, role);
            roleRequestService.createRoleRequest(roleRequest, user, role);
            return "redirect:/users/page";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "something went whrong";
        }
    }

    @ModelAttribute("isTeacher")
    public boolean populateIsTeacher(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 2);
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 3);
        }
        return false;
    }

    @ModelAttribute("isAuthorised")
    public boolean populateIsAuthorised(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 2 || roleId == 3);
        }
        return false;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }
}
