package com.telerikacademy.virtualteacher.controllers.rest;

import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.*;
import com.telerikacademy.virtualteacher.models.Dtos.AssignmentDto;
import com.telerikacademy.virtualteacher.models.Dtos.LectureDto;
import com.telerikacademy.virtualteacher.models.Dtos.NoteDto;
import com.telerikacademy.virtualteacher.services.contracts.*;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/lectures")
public class LectureController {

    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private final LectureService lectureService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final Mapper mapper;
    private final NoteService noteService;
    private final AssignmentService assignmentService;
    private final EnrolledCoursesService enrolledCoursesService;
    private final CourseService courseService;

    @Autowired
    public LectureController(LectureService lectureService, UserService userService, AuthenticationHelper authenticationHelper, Mapper mapper, NoteService noteService, AssignmentService assignmentService, EnrolledCoursesService enrolledCoursesService, CourseService courseService) {
        this.lectureService = lectureService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
        this.noteService = noteService;
        this.assignmentService = assignmentService;
        this.enrolledCoursesService = enrolledCoursesService;
        this.courseService = courseService;
    }


    @GetMapping()
    public List<Lecture> getAllLectures(@RequestHeader(required = false) HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
        return lectureService.getAllLectures();
    }

    @GetMapping("/{id}")
    public Lecture getLectureById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(headers);
            User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return lectureService.getLectureById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/title")
    public Lecture getLectureByTitle(@RequestHeader HttpHeaders headers, @RequestParam(required = false) String title) {
        try {
            authenticationHelper.tryGetUser(headers);
            User user = userService.getByEmail(headers.getFirst(AUTHORIZATION_HEADER_NAME));
            return lectureService.getLectureByTitle(title);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/create")
    public Lecture createLecture(@RequestHeader HttpHeaders headers, @Valid @RequestBody LectureDto dto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Lecture lectureToCreate = mapper.fromDto(dto);
            lectureService.createLecture(lectureToCreate, user);
            return lectureToCreate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lecture updateLecture(@RequestHeader HttpHeaders headers,
                                 @PathVariable int id, @Valid @RequestBody LectureDto dto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Lecture lectureToUpdate = mapper.fromDto(dto, id);
            lectureService.updateLecture(lectureToUpdate, user);
            return lectureToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("{id}")
    public boolean deleteLecture(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return lectureService.deleteLecture(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/add/note")
    public Note createNote(@RequestHeader HttpHeaders headers, @RequestBody NoteDto dto, @PathVariable int id){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            Note note = mapper.fromDto(dto, user);
            Lecture lectureToNote = lectureService.getLectureById(id);
            noteService.createNote(note,lectureToNote);

            Lecture lecture = mapper.setNote(id,note);
            lectureService.addNote(lecture,user);
            return note;
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/course/{courseId}/assignment/submit")
    public void submit(@RequestHeader HttpHeaders headers,@RequestParam("file") MultipartFile file,
                           @PathVariable int id, @PathVariable int courseId){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            Course course = courseService.getCourseById(courseId);
            Lecture lecture = lectureService.getLectureById(id);
            AssignmentDto assignmentDto = new AssignmentDto();
            assignmentDto.setFile(file);
            Assignment assignment = mapper.fromDto(assignmentDto,user,course,lecture);
            assignmentService.createAssignment(assignment,user,course, lecture);
            enrolledCoursesService.areAllLecturesCompleted(course,user);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }

    }

}
