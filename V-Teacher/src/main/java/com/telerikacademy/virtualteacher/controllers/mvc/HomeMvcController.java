package com.telerikacademy.virtualteacher.controllers.mvc;

import com.telerikacademy.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    @GetMapping
    public String showHomePage(){
        return "index-homepage";
    }
}
