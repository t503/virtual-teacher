package com.telerikacademy.virtualteacher.controllers.rest;

import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Assignment;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.services.contracts.AssignmentService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/assignments")
public class AssignmentController {

    private final AssignmentService assignmentService;
    private final AuthenticationHelper authenticationHelper;
    private final Mapper mapper;

    public AssignmentController(AssignmentService assignmentService, AuthenticationHelper authenticationHelper, Mapper mapper) {
        this.assignmentService = assignmentService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Assignment> getAll(@RequestHeader HttpHeaders headers) {
        try{
            User user = authenticationHelper.tryGetUser(headers);
            return assignmentService.getAll(user);
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Assignment getById(@PathVariable int id, @RequestHeader HttpHeaders headers){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            return assignmentService.getById(id, user);
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}
