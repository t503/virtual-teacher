package com.telerikacademy.virtualteacher.controllers.mvc;

import com.telerikacademy.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.RoleRequest;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.services.contracts.RoleRequestService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/role-request")
public class RoleRequestMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final RoleRequestService roleRequestService;

    @Autowired
    public RoleRequestMvcController(AuthenticationHelper authenticationHelper, RoleRequestService roleRequestService) {
        this.authenticationHelper = authenticationHelper;
        this.roleRequestService = roleRequestService;
    }

    @GetMapping("/{id}/delete")
    public String showDeleteRequest(@PathVariable int id, Model model, HttpSession session) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            roleRequestService.deleteRoleRequest(user,id);

            return "redirect:/users/approve";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/login";
        }
    }

    @GetMapping("{id}/approve")
    public String showApproveRequest(@PathVariable int id,HttpSession session, Model model){
        try{
            User user = authenticationHelper.tryGetUser(session);
            RoleRequest roleRequest = roleRequestService.getRoleRequestById(id);
            roleRequestService.approveRequest(user,roleRequest);
            return "redirect:/users/approve";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }
}
