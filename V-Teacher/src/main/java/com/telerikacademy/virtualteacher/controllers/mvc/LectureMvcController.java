package com.telerikacademy.virtualteacher.controllers.mvc;

import com.telerikacademy.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Assignment;
import com.telerikacademy.virtualteacher.models.Dtos.AssignmentDto;
import com.telerikacademy.virtualteacher.models.Dtos.LectureDto;
import com.telerikacademy.virtualteacher.models.Lecture;
import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.services.contracts.AssignmentService;
import com.telerikacademy.virtualteacher.services.contracts.FileService;
import com.telerikacademy.virtualteacher.services.contracts.LectureService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/lectures")
public class LectureMvcController {

    public static final String UNAUTHORIZED_CUSTOMER_ENTRY_ERROR_MESSAGE = "A customer cannot enter the employee section";

    private final LectureService lectureService;
    private final AssignmentService assignmentService;
    private final AuthenticationHelper authenticationHelper;
    private final FileService fileService;
    private final Mapper mapper;

    @Autowired
    public LectureMvcController(LectureService lectureService, AssignmentService assignmentService,
                                AuthenticationHelper authenticationHelper, FileService fileService, Mapper mapper) {
        this.lectureService = lectureService;
        this.assignmentService = assignmentService;
        this.authenticationHelper = authenticationHelper;
        this.fileService = fileService;
        this.mapper = mapper;
    }

    @ModelAttribute("isTeacher")
    public boolean populateIsTeacher(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 2);
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 3);
        }
        return false;
    }

    @ModelAttribute("isAuthorised")
    public boolean populateIsAuthorised(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 2 || roleId == 3);
        }
        return false;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/create")
    public String showCreateLecture(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            LectureDto lectureDto = new LectureDto();
            model.addAttribute("lectureDto", lectureDto);
            model.addAttribute("user", user);
            return "lecture-create";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/create")
    public String createLecture(@Valid @ModelAttribute("lectureDto") LectureDto lectureDto,
                               BindingResult bindingResult, HttpSession session, Model model) {
        if (bindingResult.hasErrors()) {
            return "lecture-create";
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            Lecture lecture = mapper.fromDto(lectureDto);
            lectureService.createLecture(lecture, user);
            model.addAttribute("lecture", lecture);
            return "lecture-by-id";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (EntityNotFoundException e){
            model.addAttribute("error",e.getMessage());
            return "404";
        }
    }




}
