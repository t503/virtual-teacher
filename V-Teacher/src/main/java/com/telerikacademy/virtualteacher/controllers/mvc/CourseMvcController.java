package com.telerikacademy.virtualteacher.controllers.mvc;

import com.telerikacademy.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualteacher.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.Course;
import com.telerikacademy.virtualteacher.models.Dtos.CourseDto;
import com.telerikacademy.virtualteacher.models.Dtos.PhotoMvcDto;
import com.telerikacademy.virtualteacher.models.Topic;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.models.*;
import com.telerikacademy.virtualteacher.models.Dtos.*;
import com.telerikacademy.virtualteacher.services.contracts.*;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/courses")
public class CourseMvcController {

    public static final String ONLY_ADMINS_AND_TEACHERS_CNA_GRADE_ASSIGNMENTS_ERROR_MESSAGE = "Only Admins and teachers cna grade assignments";
    private final AuthenticationHelper authenticationHelper;
    private final CourseService courseService;
    private final TopicService topicService;
    private final AssignmentService assignmentService;
    private final Mapper mapper;
    private final FileService fileService;
    private final LectureService lectureService;
    private final EnrolledCoursesService enrolledCoursesService;
    private final UserService userService;

    @Autowired
    public CourseMvcController(AuthenticationHelper authenticationHelper, CourseService courseService,
                               TopicService topicService, AssignmentService assignmentService, Mapper mapper,
                               FileService fileService, LectureService lectureService,
                               EnrolledCoursesService enrolledCoursesService, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.courseService = courseService;
        this.topicService = topicService;
        this.assignmentService = assignmentService;
        this.mapper = mapper;
        this.fileService = fileService;
        this.lectureService = lectureService;
        this.enrolledCoursesService = enrolledCoursesService;
        this.userService = userService;
    }

    @ModelAttribute("isTeacher")
    public boolean populateIsTeacher(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 2);
        }
        return false;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 3);
        }
        return false;
    }

    @ModelAttribute("isAuthorised")
    public boolean populateIsAuthorised(HttpSession session) {
        if (populateIsAuthenticated(session)) {
            User user = authenticationHelper.tryGetUser(session);
            return user.getUserRole().stream().map(Role::getId).anyMatch(roleId -> roleId == 2 || roleId == 3);
        }
        return false;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("topics")
    public List<Topic> topicList() {
        return topicService.getAllTopics();
    }

    @GetMapping("/all")
    public String showAllCourses(Model model) {
        try {
            model.addAttribute("courses", courseService.getAllPublishedCourses());
            return "all-courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/all/sort")
    public String showAllSortedCourses(Model model) {
        try {
            model.addAttribute("courses", courseService.getAllPublishedCourses());
            model.addAttribute("sortedCourses", new SortMvcDto());
            return "all-courses-sort";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @PostMapping("/all/sort")
    public String sortCourse(@ModelAttribute("sortedCourses") SortMvcDto sortDto, Model model) {
        try {
            List<Course> sortedCourses = courseService.sortedMvcCourses(sortDto.getSort());
            model.addAttribute("courses", sortedCourses);
            return "all-courses-sort";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (IndexOutOfBoundsException e) {
            model.addAttribute("error", e.getMessage());
            return "something-went-wrong";
        }
    }

    @GetMapping("/all/filter")
    public String showAllFilteredCourses(Model model) {
        try {
            model.addAttribute("courses", courseService.getAllPublishedCourses());
            model.addAttribute("courseSearchDto", new SearchMvcDto());
            return "all-courses-filter";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @PostMapping("/all/filter")
    public String searchForCourse(@ModelAttribute("courseSearchDto") SearchMvcDto searchDto, Model model) {
        try {
            List<Course> courses = courseService.searchCourseByMVC(searchDto.getSearchBy(), searchDto.getValue());
            model.addAttribute("courses", courses);
            return "all-courses-filter";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (IndexOutOfBoundsException e) {
            model.addAttribute("error", e.getMessage());
            return "something-went-wrong";
        }
    }

    @GetMapping("/{id}")
    public String showCourseByIdPage(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = courseService.getCourseById(id);
            session.setAttribute("user", user);
            model.addAttribute("course", course);
            model.addAttribute("courseTeachers", course.getCourseTeachers());
            model.addAttribute("allTeachers", userService.getAllUnassignedTeachers(course));
            model.addAttribute("addTeacherDto", new AddTeacherMvcDto());
            model.addAttribute("isCourseCompleted", isCourseCompleted(course, user));

            return "course-by-id";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/{id}")
    public String courseByIdPage(@PathVariable int id,
                                 @Valid @ModelAttribute("addTeacherDto") AddTeacherMvcDto addTeacherMvcDto,
                                 HttpSession session, Model model) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            courseService.addTeacher(addTeacherMvcDto.getTeacherId(), id, user);
            return String.format("redirect:/courses/%d", id);
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/create")
    public String showCreateCourse(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            CourseDto courseDto = new CourseDto();
            model.addAttribute("courseDto", courseDto);
            model.addAttribute("user", user);
            return "course-create";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/create")
    public String createCourse(@Valid @ModelAttribute("courseDto") CourseDto courseDto,
                               BindingResult bindingResult, HttpSession session, Model model) {
        if (bindingResult.hasErrors()) {
            return "course-create";
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = mapper.fromDto(courseDto);
            courseService.createCourse(course, user);
            return String.format("redirect:/courses/%d", course.getId());
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/{id}/upload/picture")
    public String showUploadCoursePhoto(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("course", courseService.getCourseById(id));
            model.addAttribute("newPhoto", new PhotoMvcDto());

            return "course-upload-photo";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/{id}/upload/picture")
    public String uploadCoursePhoto(@PathVariable int id, HttpSession session,
                                    @Valid @ModelAttribute("newPhoto") PhotoMvcDto userPhotoDto,
                                    Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = courseService.getCourseById(id);
            MultipartFile file = userPhotoDto.getFile();
            String picture = fileService.saveFile(file);
            course.setPicture(picture);
            courseService.updateCourse(course, user);
            return "redirect:/users/courses/all";

        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }


    @GetMapping("/{id}/update")
    public String showUpdateCourse(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = courseService.getCourseById(id);
            UpdateCourseDto courseDto = mapper.toUpdateCourseDto(course);
            model.addAttribute("courseDto", courseDto);
            model.addAttribute("user", user);
            return "course-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("courseDto") UpdateCourseDto dto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        if (errors.hasErrors()) {
            return "course-update";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = mapper.fromDto(dto, id);
            courseService.updateCourse(course, user);
            return String.format("redirect:/courses/%d", course.getId());
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }


    @GetMapping("/{id}/lectures")
    public String showAddLectureToCourse(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = courseService.getCourseById(id);

            model.addAttribute("courseLectures", courseService.getCourseById(id).getCourseLectures());
            model.addAttribute("lectureDto", new AddLectureDto());
            model.addAttribute("user", user);
            model.addAttribute("allLectures", lectureService.getAllLectures());
            model.addAttribute("courseToAddLecture", course);

            return "course-add-lectures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/{id}/lectures")
    public String updateCourseLectures(@PathVariable int id,
                                       @ModelAttribute("lectureDto") AddLectureDto dto,
                                       BindingResult errors,
                                       Model model,
                                       HttpSession session) {

        if (errors.hasErrors()) {
            return "course-add-lectures";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = courseService.getCourseById(id);
            Lecture lecture = lectureService.getLectureById(dto.getLectureId());
            Set<Lecture> lectureSet = course.getCourseLectures();
            lectureSet.add(lecture);
            course.setCourseLectures(lectureSet);

            courseService.updateCourse(course, user);
            model.addAttribute("courseLectures", courseService.getCourseById(id).getCourseLectures());
            model.addAttribute("allLectures", lectureService.getAllLectures());
            model.addAttribute("courseToAddLecture", course);
            return "course-add-lectures";

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("{id}/rate")
    public String showCourseRateView(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course courseToRate = courseService.getCourseById(id);
            model.addAttribute("courseToRate", courseToRate);
            model.addAttribute("rateDto", new RatingDto());
            model.addAttribute("courseTeachers", userService.getAllTeachers());
            model.addAttribute("allTeachers", userService.getAllUnassignedTeachers(courseToRate));
            model.addAttribute("isCourseCompleted", isCourseCompleted(courseToRate, user));
            return "rate-course";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("{id}/rate")
    public String rateCourse(@PathVariable int id, @ModelAttribute("rateDto") RatingDto rateDto,
                             HttpSession session, Model model) {
        try {
            var user = authenticationHelper.tryGetUser(session);
            var course = courseService.getCourseById(id);
            enrolledCoursesService.rate(course, user, rateDto.getRating());
            return String.format("redirect:/courses/%d", course.getId());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("{courseId}/lectures/{id}")
    public String showLectureByIdPage(@PathVariable int courseId, @PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = courseService.getCourseById(courseId);
            Lecture lecture = lectureService.getLectureById(id);

            session.setAttribute("user", user);

            model.addAttribute("lecture", lectureService.getLectureById(id));
            model.addAttribute("assignment", new AssignmentDto());
            model.addAttribute("course", courseService.getCourseById(courseId));
            model.addAttribute("enrolledCourses", enrolledCoursesService.getUserCompletedEnrolledCourses(user));
            model.addAttribute("hasUserSubmitted", userHasSubmitted(user, course, lecture));

            return "lecture-by-id";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("{courseId}/lectures/{id}")
    public String uploadLectureAssignment(@PathVariable int courseId, @PathVariable int id,
                                          HttpSession session,
                                          Model model,
                                          @Valid @ModelAttribute("assignment") AssignmentDto assigmentDto) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Assignment assignment = new Assignment();
            Course course = courseService.getCourseById(courseId);
            Lecture lecture = lectureService.getLectureById(id);
            EnrolledCourses enrolledCourse = enrolledCoursesService.getByUserAndCourse(user, course);

            MultipartFile file = assigmentDto.getFile();
            String filePath = fileService.saveFile(file);

            assignment.setCourse(course);
            assignment.setLecture(lecture);
            assignment.setUser(user);
            assignment.setFilePath(filePath);
            assignment.setComplete(true);
            assignmentService.createAssignment(assignment, user, course, lecture);

            if (enrolledCoursesService.areAllLecturesCompleted(course, user)) {
                enrolledCourse.setComplete(true);
                enrolledCoursesService.update(enrolledCourse);
            }


            model.addAttribute("course", course);
            model.addAttribute("lecture", lecture);
            model.addAttribute("hasUserSubmitted", userHasSubmitted(user, course, lecture));

            return "lecture-by-id";

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCourse(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Course course = courseService.getCourseById(id);
            List<Assignment> courseAssignments = assignmentService.getAssignmentsByCourse(course,user);
            if (courseAssignments.size() != 0){
                for (Assignment assignment: courseAssignments) {
                    assignmentService.deleteAssignment(assignment,user);
                }
            }
            List<EnrolledCourses> courseEnrolls = enrolledCoursesService.getByCourse(course,user);
            if (courseEnrolls.size() != 0){
                for (EnrolledCourses enrolledCourses: courseEnrolls) {
                    enrolledCoursesService.deleteEnrolledCourse(enrolledCourses,user);
                }
            }
            courseService.deleteCourse(id,user);
            return "redirect:/users/courses/all";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("{courseId}/lectures/{id}/user/{userId}/assignment")
    public String gradeAssignment(@PathVariable int courseId, @PathVariable int id, @PathVariable int userId,
                                  HttpSession session,
                                  Model model) {

        try {
            authenticationHelper.tryGetUser(session);
            User user = userService.getUserById(userId);
            Course course = courseService.getCourseById(courseId);
            Lecture lecture = lectureService.getLectureById(id);
            Assignment assignment = assignmentService.getAssignment(user, course, lecture);
            User student = assignment.getUser();
            GradeDto gradeDto = new GradeDto();

            model.addAttribute("course", course);
            model.addAttribute("lecture", lecture);
            model.addAttribute("assignment", assignment);
            model.addAttribute("gradeDto", gradeDto);
            model.addAttribute("student", student);

            return "assignment-open-view";

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("{courseId}/lectures/{id}/user/{userId}/assignment")
    public String gradeAssignment(@PathVariable int courseId, @PathVariable int id, @PathVariable int userId,
                                  HttpSession session,
                                  Model model,
                                  @Valid @ModelAttribute("gradeDto") GradeDto gradeDto,
                                  BindingResult errors) {
        try {
            User userRequesting = authenticationHelper.tryGetUser(session);
            User user = userService.getUserById(userId);
            if (user.isOnlyStudent()){
                throw new UnauthorizedOperationException(ONLY_ADMINS_AND_TEACHERS_CNA_GRADE_ASSIGNMENTS_ERROR_MESSAGE);
            }
            Course course = courseService.getCourseById(courseId);
            Lecture lecture = lectureService.getLectureById(id);
            Assignment assignment = assignmentService.getAssignment(user, course, lecture);
            User student = assignment.getUser();

            model.addAttribute("course", course);
            model.addAttribute("lecture", lecture);
            model.addAttribute("assignment", assignment);
            model.addAttribute("gradeDto", gradeDto);
            model.addAttribute("student", student);

            if (errors.hasErrors()) {
                return "assignment-open-view";
            }

            assignment.setUserGrade(gradeDto.getGrade());
            assignmentService.updateAssignment(assignment, userRequesting);
            enrolledCoursesService.calculateCourseFinalGrade(user, course);

            return "assignment-open-view";

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @GetMapping("/assignments")
    public String getAssignmentsForGrading(HttpSession session, Model model) {

        try {
            User user = authenticationHelper.tryGetUser(session);

            List<Assignment> assignments = assignmentService.getAssignmentsPendingForGrading(user);
            model.addAttribute("assignments", assignments);
            model.addAttribute("searchAssignmentDto", new SearchMvcDto());

            return "assignments-pending-for-grading";

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        }
    }

    @PostMapping("/assignments")
    public String getFilteredAssignments(@ModelAttribute("searchAssignmentDto") SearchMvcDto searchDto, Model model) {
        try {
            List<Assignment> assignments = assignmentService.getFilterAssignments(searchDto.getSearchBy(), searchDto.getValue());
            model.addAttribute("assignments", assignments);
            model.addAttribute("searchAssignmentDto", new SearchMvcDto());
            return "assignments-pending-for-grading";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "401";
        } catch (IndexOutOfBoundsException e) {
            model.addAttribute("error", e.getMessage());
            return "something-went-wrong";
        }
    }

    private boolean userHasSubmitted(User user, Course course, Lecture lecture) {
        return assignmentService.hasUserSubmitted(user, course, lecture);
    }

    private boolean isCourseCompleted(Course course, User user) {
        List<EnrolledCourses> completedEnrolledCourses = enrolledCoursesService.getUserCompletedEnrolledCourses(user);
        List<Course> completedCourses = new ArrayList<>();
        for (EnrolledCourses enrolledCourse : completedEnrolledCourses) {
            Course cors = enrolledCourse.getCourse();
            completedCourses.add(cors);
        }
        return completedCourses.contains(course);
    }
}
