package com.telerikacademy.virtualteacher.controllers.mvc;

import com.telerikacademy.virtualteacher.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualteacher.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualteacher.models.Role;
import com.telerikacademy.virtualteacher.models.RoleRequest;
import com.telerikacademy.virtualteacher.models.User;
import com.telerikacademy.virtualteacher.models.Dtos.LoginDto;
import com.telerikacademy.virtualteacher.models.Dtos.RegisterUserDto;
import com.telerikacademy.virtualteacher.services.contracts.RoleRequestService;
import com.telerikacademy.virtualteacher.services.contracts.RoleService;
import com.telerikacademy.virtualteacher.services.contracts.UserService;
import com.telerikacademy.virtualteacher.utils.AuthenticationHelper;
import com.telerikacademy.virtualteacher.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class AuthenticationMvcController {

    public static final String PASSWORD_CONFIRMATION_SHOULD_MATCH_PASSWORD = "Password confirmation should match password.";
    public static final String ERROR_PASSWORD = "error.password";
    public static final String CONFIRM_PASSWORD = "confirmPassword";
    public static final String DUPLICATE_CUSTOMER_USERNAME = "duplicate_customer.username";
    public static final String DUPLICATE_CUSTOMER_EMAIL = "duplicate_customer.email";
    public static final String ERROR_AUTH = "error.auth";
    private final UserService userService;
    private final Mapper mapper;
    private final AuthenticationHelper authenticationHelper;
    private final RoleService roleService;
    private final RoleRequestService roleRequestService;

    @Autowired
    public AuthenticationMvcController(UserService userService, Mapper mapper, AuthenticationHelper authenticationHelper, RoleService roleService, RoleRequestService roleRequestService) {
        this.userService = userService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
        this.roleService = roleService;
        this.roleRequestService = roleRequestService;
    }

    @GetMapping("/register")
    public String showNewCustomerPage(Model model) {
        model.addAttribute("student", new RegisterUserDto());
        return "register";
    }

    @PostMapping("/register")
    public String createCustomer(@Valid @ModelAttribute("student") RegisterUserDto registerUserDto, BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        if (!registerUserDto.getPassword().equals(registerUserDto.getConfirmPassword())) {
            bindingResult.rejectValue(CONFIRM_PASSWORD, ERROR_PASSWORD, PASSWORD_CONFIRMATION_SHOULD_MATCH_PASSWORD);
            return "register";
        }

        try {
            User user = mapper.fromDto(registerUserDto);
            userService.createUser(user);
            if(registerUserDto.getRoleId() == 2){
                RoleRequest roleRequest = new RoleRequest();
                roleRequest.setUser(user);
                roleRequest.setRole(roleService.getRoleById(2));
                roleRequestService.createRoleRequest(roleRequest,user,roleService.getRoleById(2));
            }

            return "redirect:/login";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("username")) {
                bindingResult.rejectValue("username", DUPLICATE_CUSTOMER_USERNAME, e.getMessage());
            }
            if (e.getMessage().contains("email")) {
                bindingResult.rejectValue("email", DUPLICATE_CUSTOMER_EMAIL, e.getMessage());
            }
            return "register";
        }
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(login.getEmail(), login.getPassword());
            session.setAttribute("currentUser", login.getEmail());
            session.setAttribute("user", user);
            return "redirect:/users/page";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("email", ERROR_AUTH, e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @ModelAttribute("allowedRolesForRegistration")
    public List<Role> populateRole() {
        List<Role> allowedRolesForRegistration = new ArrayList<>();
        for (int i = 1; i < 3; i++) {
            Role role = roleService.getRoleById(i);
            allowedRolesForRegistration.add(role);
        }
        return allowedRolesForRegistration;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

}
